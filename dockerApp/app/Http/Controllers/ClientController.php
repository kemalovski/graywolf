<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Elasticsearch\ClientBuilder;
use Elastica\Client as ElasticaClient;

class ClientController extends Controller
{
    // Elasticsearch-php Client
    protected $elasticsearch;

    // Elastica Client
    protected $elastica;

    // Set up our clients

    public function __construct() {

    	$this->elasticsearch = ClientBuilder::create()->build();

    	// Create an Elastica client
    	$elasticaConfig = [
    		'host' => 'localhost',
    		'port' => 9200,
    		'index'=> 'kibana_sample_data_ecommerce'
    	];

    	$this->elastica = new ElasticaClient($elasticaConfig);

    	
    }

    public function elasticsearchTest() {
    	// View our elasticsearch-php client object
    	dump($this->elasticsearch);

    	// Retrieve a document that we have indexed
    	echo "\n\n Retrieve a document:\n";

    	exit();

    	$params = [
    		'index' => 'kibana_sample_data_ecommerce',
    		'type' => 'bag',
    		'id' => '1'
    	];

    	$response = $this->elasticsearch->get($params);
    	dump($response);
    }
}
