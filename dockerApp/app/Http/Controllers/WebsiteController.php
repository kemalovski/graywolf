<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Page;
use App\Menu;
use Carbon\Carbon;
// use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;

class WebsiteController extends Controller
{
    
    public function index()
    {

        // $osman = Cache::remember('website.index', 60, function() {
        //     return View::make('website.index')
        //         ->render();
        // });

        // return $osman;
        
        return view( 'personalWebsite.app', [ 'blogs' => Blog::orderBy('created_at','asc')->get() ] );
        // return view( 'personalWebsite.app');
    }

    public function category($categoryUrl)
    {

        Menu::where('url', $categoryUrl)->first() ?? abort(404);
        
        return view('website.category', array( 'menu' => Menu::where('url', $categoryUrl)->first(), 'Carbon' => 'Carbon\Carbon' ) );

    }

    public function content($menu, $blog)
    {

        return view('website.content', array( 'blog' => Blog::where('url', $blog)->first(), 'menu' => Menu::where('url', \Request::segment(1) )->first() ) );

    }

    public function page($page)
    {
        return view('website.page',  array( 'menu' => Menu::where('url', $page)->first() ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
