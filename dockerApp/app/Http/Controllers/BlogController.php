<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Blog;
use App\BlogCategory;
use App\ImageGallery;
use App\Http\Controllers\UsersLogsController;
use Session;


class BlogController extends Controller
{
    
    public function showForm()
    {
        $blogcategories = BlogCategory::pluck('title', 'id');

        $imageGalleries = ImageGallery::pluck('title', 'id');

    	return view( 'dashboard.blog.showForm', compact('blogcategories', 'imageGalleries') );
    }

    public function save(Request $request, UsersLogsController $usersLogs)
    {

		$this->validationSaveBlog($request);

        $imageName = $request['url'].'.'.request()->imageFile->getClientOriginalExtension();

        $request->merge(['image' => $imageName]);

        request()->imageFile->move(public_path('images/blogs'), $imageName);


    	$blog = Blog::create($request->all());

        $blog->BlogCategory()->attach( explode(",",$request['categories']) );

        $usersLogs->store($request, $request['title'].' başlıklı blogu kayıt etti.');

        Session::flash('message', 'Blog kayıt oldu!');

        return back();
    }

    public function validationSaveBlog($request)
    {
        return $this->validate( $request, 
            [
                'title' => 'required|max:100',
                'url' => 'required|unique:blogs|max:150',
                'imageFile' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
                'content' => 'required',
                'categories' => 'required|max:50',
                'content' => 'required',
                'tags' => 'max:300',
                'description' => 'required|max:1000',
                'subtitle' => 'required|max:1000'
            ]
            ,[
                'subtitle.required' => 'Alt başlığı boş bırakmayınız.',
                'title.required' => 'Başlığı boş bırakmayınız.', 
                'title.max' => 'Başlık 100 karakterden uzun yazmayınız.', 
                'url.unique' => 'Uzantı zaten kayıtlı.', 
                'url.max' => 'Uzantı 150 karakterden uzun yazmayınız.', 
                'url.required' => 'Uzantıyı boş bırakmayınız.', 
                'imageFile.required' => 'Resmi boş bırakmayınız.', 
                'imageFile.max' => 'Resmin boyutu çok büyük.', 
                'content.required' => 'İçeriği boş bırakmayınız.', 
                'tags.max' => 'Etiketler 100 karakterden fazla olamaz.', 
                'description.max' => 'Açıklamalar 255 karakterden fazla olamaz.', 
                'description.required' => 'Açıklamayı boş bırakmayınız.', 
                'categories.required' => 'Kategoriyi boş bırakmayınız.', 
                'description.max' => 'Açıklama 1000 karakterden fazla olamaz.', 
            ]
        );
    }

    public function editBlog($id, Request $request, UsersLogsController $usersLogs)
    {

        $this->validationEditBlog($request, $id);


        $blog= Blog::find($id);
        
        if (request()->imageFile) {

            if( file_exists( public_path('images/blogs') . '/' . $blog->image ) ){

                unlink( public_path('images/blogs') . '/' . $blog->image );

            }

            $image = $request['url'].'.'.request()->imageFile->getClientOriginalExtension();

            $request->merge(['image' => $image]);

            request()->imageFile->move(public_path('images/blogs'), $image);

            $blog->image = $request->get('image');

        }

        $blog->title = $request->get('title');
        $blog->url = $request->get('url');
        $blog->description = $request->get('description');
        $blog->content = $request->get('content');
        $blog->subtitle = $request->get('subtitle');
        $blog->categories = $request->get('categories');
        $blog->tags = $request->get('tags');
        $blog->image_gallery_id = $request->get('image_gallery_id');

        $blog->save();

        $blog->BlogCategory()->sync( explode( ",", $request->get('categories') ) );

        $usersLogs->store($request, $blog->title.' başlıklı blogu değiştirildi.');

        Session::flash('message', 'Blog değiştirildi!');

        return back();
    }

    public function validationEditBlog($request, $id)
    {
        return $this->validate( $request, 
            [
                'title' => 'required|max:100',
                'url' => 'required|max:150|unique:blogs,url,'.$id,
                'imageFile' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'content' => 'required',
                'categories' => 'required|max:50',
                'content' => 'required',
                'tags' => 'max:300',
                'description' => 'required|max:1000'
            ]
            ,[
                'title.required' => 'Başlığı boş bırakmayınız.', 
                'title.max' => 'Başlık 100 karakterden uzun yazmayınız.', 
                'url.unique' => 'Uzantı zaten kayıtlı.', 
                'url.max' => 'Uzantı 150 karakterden uzun yazmayınız.', 
                'url.required' => 'Uzantıyı boş bırakmayınız.', 
                'imageFile.required' => 'Resmi boş bırakmayınız.', 
                'imageFile.max' => 'Resmin ismi 100 karakterden uzun olamaz.', 
                'content.required' => 'İçeriği boş bırakmayınız.', 
                'tags.max' => 'Etiketler 100 karakterden fazla olamaz.', 
                'description.max' => 'Açıklamalar 255 karakterden fazla olamaz.', 
                'description.required' => 'Açıklamayı boş bırakmayınız.', 
                'categories.required' => 'Kategoriyi boş bırakmayınız.', 
                'description.max' => 'Açıklama 1000 karakterden fazla olamaz.', 
            ]
        );
    }
    
    public function view()
    {
        return view('dashboard.blog.view', ['blogs' => Blog::orderBy('created_at','asc')->get()] );
    }

    public function remove(Request $request, UsersLogsController $usersLogs)
    {
        if(isset($request['id'])){

            $blog = Blog::find($request['id']);
            
            if( file_exists( public_path('images/blogs') . '/' . $blog->image ) ){

                unlink( public_path('images/blogs') . '/' . $blog->image );

            }

            $blog->BlogCategory()->detach( explode( ",", $blog->categories ) );

            $usersLogs->store($request, $blog->title.' başlıklı blogu sildi.');

            $blog->delete();

            Session::flash('message', $request['id'].' id\' li blog silindi.');

            return back();

        }
    }
    public function showEditForm($id)
    {

        $blog = Blog::find($id);

        // return $blog->ImageGallery->images;

        $blogcategories = BlogCategory::pluck('title', 'id');

        $imageGalleries = ImageGallery::pluck('title', 'id');

        return view( 'dashboard.blog.showEditForm', compact( 'blog', 'id', 'blogcategories', 'imageGalleries' ) );

    }
}






