<?php

namespace App\Http\Controllers;

use App\BlogCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\UsersLogsController;
use Session;

class BlogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.blog.categoryIndex');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UsersLogsController $usersLogs)
    {


        $this->validator($request);

        $imageName = $request['url'].'.'.request()->imageFile->getClientOriginalExtension();

        $request->merge(['image' => $imageName]);

        $oldumu = request()->imageFile->move(public_path('images/blogCategories'), $imageName);

        $stored = BlogCategory::create([
            'title' => $request['title'],
            'subtitle' => $request['subtitle'],
            'description' => $request['description'],
            'image' => $request['image'],
            'url' => $request['url'],
        ]);

        if ($stored) {
            $usersLogs->store($request, $request['title'].' başlıklı blog kategorisi kayıt etti.');
            Session::flash('message', 'Blog kategorisi kayıt oldu!');
        }else {
            Session::flash('message', 'Bir sorun var Kayıt edemedik!');    
        }

        return back();
    }

    protected function validator($request, $id = 0)
    {
        return $this->validate( $request, 
            [
                'title' => 'required|max:100|unique:blog_categories,title,'.$id,
                'subtitle' => 'required|max:400',
                'description' => 'required'
            ]
            ,[
                'title.required' => 'Başlığı boş bırakmayınız.', 
                'title.max' => 'Başlık 100 karakterden uzun yazmayınız.',
                'title.unique' => 'Bu kategori zaten kayıtlı.',
                'subtitle.required' => 'Alt başlığı boş bırakmayınız.',
                'subtitle.max' => 'Altbaşlık 400 karakterden fazla olmamalı.',
                'description.required' => 'Açıklamayı boş bırakmayınız.',

            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function show(BlogCategory $blogCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blogCategory = BlogCategory::find($id);
        return view('dashboard.blog.showEditBlogCategoryForm',compact('blogCategory','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, UsersLogsController $usersLogs)
    {
        $this->validator($request, $id);

        $blogCategory = BlogCategory::find($id); 


        

        if (request()->imageFile) {

            if( file_exists( public_path('images/blogCategories') . '/' . $blogCategory->image ) ){

                unlink( public_path('images/blogCategories') . '/' . $blogCategory->image );

            }

            $image = $request->get('url').'.'.request()->imageFile->getClientOriginalExtension();

            $request->merge(['image' => $image]);

            request()->imageFile->move(public_path('images/blogCategories'), $image);

            $blogCategory->image = $request->get('image');

        }

        $usersLogs->store($request, $request['title'].' başlıklı blog kategorisini değiştirdi.');

        $blogCategory->title = $request->get('title');
        $blogCategory->subtitle = $request->get('subtitle');
        $blogCategory->description = $request->get('description');
        $blogCategory->url = $request->get('url');

        $blogCategory->save();

        Session::flash('message', 'Blog Kategorisi değiştirildi!');

        return redirect('/graywolf/blog/kategoriler');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, UsersLogsController $usersLogs)
    {

        // BlogCategory::destroy($request['id']);

        $blogCategory = BlogCategory::find($request['id']);


        if( file_exists( public_path('images/blogCategories') . '/' . $blogCategory->image ) ){

            unlink( public_path('images/blogCategories') . '/' . $blogCategory->image );

        }

        $usersLogs->store($request, $blogCategory->title.' başlıklı blog kategorisini sildi.');

        $blogCategory->delete();
        
        Session::flash('message', $request['id'].' id\' li blog silindi.');

        return back();
    }
}














