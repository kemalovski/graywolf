@extends('dashboard.app')

@section('content')
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Ürün</h1>
		<p>
		  <a class="btn btn-secondary"  href="{{ route('products.index') }}">
		    Ürünleri görüntüle
		  </a>
		  <a class="btn btn-secondary disabled"  href="{{ route('products.create') }}">
		    Ürünleri Kayıt Et
		  </a>
		</p>
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="dashboard">Ürün</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Kayıt etme sayfası</li>
		  </ol>
		</nav>
	</div>
	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	@if(Session::has('message'))
	<div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true" style="font-size:20px">×</span>
	  	</button>
	  	{{ Session::get('message') }}
	</div>
	@endif
	@if(isset($product))
	  {!! Form::open([ 'method' => 'PATCH', 'novalidate', 'id' => 'nee ds-validation', 'enctype' =>"multipart/form-data", 'action' => ['ProductController@update', $product->id] ]) !!}
	@else
	  {!! Form::open([ 'method' => 'POST', 'novalidate', 'id' => 'needs-validation', 'enctype' =>"multipart/form-data", 'action' => 'ProductController@store' ]) !!}
	@endif
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					{{ Form::label( 'title', 'Başlık', [] ) }}
					{{ Form::text('title', isset($product) ? $product->title : old('title'), ['placeholder' => 'Başlık giriniz', 'maxlength' => '100', 'class' => 'form-control', 'id' => 'title', 'required'] ) }}
	                <div class="invalid-feedback">
	                	Başlığı boş bırakmayınız.
	                </div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					{{ Form::label( 'url', 'Uzantı', [] ) }}
					{{ Form::text('url', isset($product) ? $product->url : old('url'), ['placeholder' => 'Uzantı kendiliğinden oluşacaktır', 'maxlength' => '150', 'class' => 'form-control', 'id' => 'url', 'readonly'] ) }}
					<div class="invalid-feedback">
	                	Uzantıyı boş bırakmayınız.
	                </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					{{ Form::label( 'subtitle', 'Alt Başlık', [] ) }}
					{{ Form::text('subtitle', isset($product) ? $product->subtitle : old('subtitle'), ['placeholder' => 'Alt başlık giriniz.', 'maxlength' => '150', 'class' => 'form-control', 'id' => 'subtitle', 'required'] ) }}
					<div class="invalid-feedback">
	                	Alt başlığı boş bırakmayınız.
	                </div>
				</div>
			</div>
			{{-- <div class="col-lg-6">
				<div class="form-group">
					<label for="categories">Kategoriler</label>
					<input type="text" maxlength="50" class="form-control" name="categories" id="categories" data-paraia-multi-select="true" placeholder="Kategori Seçiniz">

					<div class="invalid-feedback">
		            	Kategori seçiniz.
		            </div>
				</div>
			</div> --}}
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
			        {{ Form::label( 'imageFile', 'Resim', [] ) }}
			        <div class="input-group">
			            <span class="input-group-prepend">
			                <span class="btn btn-outline-secondary btn-file">
			                    Resim seçiniz <input class="form-control" type="file" id="imageFile" name="imageFile">
			                </span>
			            </span>
			            <input type="text" class="form-control" value="{{ isset($product) ? $product->image : '' }}" name="imgInp" id="imgInp" required>
			            <div class="invalid-feedback">
		                	Resmi boş bırakmayınız
		                </div>
			        </div>
			        <img id='img-upload' src="{{ isset($product) ? url('/').'/images/products/'.$product->image : ''  }}"  />
			    </div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="image_gallery_id">Resim Galerisi</label>
					<select class="form-control" id="image_gallery_id" name="image_gallery_id">
				      <option value="">Resim Galerisini seçebilirsiniz</option>
				      @foreach( $imageGalleries as $key => $imageGallery )
				      	<option value="{{$key}}"  
				      		@if (isset($product->ImageGallery->id))
				      			{{$key == $product->ImageGallery->id ? 'selected' : ''}} 
				      		@endif
				      	>{{$imageGallery}}</option>
				      @endforeach
				    </select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					{{ Form::label( 'content', 'İçerik', [] ) }}
					<div id="editor">
						{{ Form::textarea('content', isset($product) ? $product->content : old('content'), ['class' => 'form-control', 'id' => 'content', 'required'] ) }}
						<div class="invalid-feedback">
		                	İçeriksiz internet sayfası mı olurmuş?
		                </div>
					</div>
			  	</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					{{ Form::label( 'description', 'Google için Açıklama', [] ) }}
					{{ Form::text('description', isset($product) ? $product->description : old('description'), ['placeholder' => 'Açıklama giriniz.', 'maxlength' => '150', 'class' => 'form-control', 'id' => 'description', 'required'] ) }}
					<div class="invalid-feedback">
	                	Açıklamayı boş bırakmayınız.
	                </div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					{{ Form::label( 'tags', 'Etiketler', [] ) }}
					{{ Form::text('tags', isset($product) ? $product->tags : old('tags'), ['placeholder' => 'Etiket giriniz.', 'data-role' => 'tagsinput', 'maxlength' => '300', 'class' => 'form-control', 'id' => 'tags', 'required'] ) }}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::submit('Kaydet', ['class' => 'btn btn-info col-lg-6', 'id' => 'buttonSaveForm']) }}
			</div>
		</div>	
	{!! Form::close() !!}
@endsection

@section('addcss')

	{{-- multiselect başladı --}}
    <link rel="stylesheet" href="/css/paraia_multi_select.min.css">

	{{-- multiselect bitti --}}

	<link rel="stylesheet" href="/css/tagsinput.css">
	
	<link rel="stylesheet" href="/css/richTextEditor/froala_editor.css">
	<link rel="stylesheet" href="/css/richTextEditor/froala_style.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/code_view.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/draggable.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/colors.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/emoticons.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/image_manager.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/image.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/line_breaker.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/table.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/char_counter.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/video.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/fullscreen.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/file.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/quick_insert.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/help.css">
	<link rel="stylesheet" href="/css/richTextEditor/third_party/spell_checker.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/special_characters.css">
	<link rel="stylesheet" href="/css/richTextEditor/codemirror.min.css">
	
	<style type="text/css">
		.btn-file {
		    position: relative;
		    overflow: hidden;
		}
		.btn-file input[type=file] {
		    position: absolute;
		    top: 0;
		    right: 0;
		    min-width: 100%;
		    min-height: 100%;
		    font-size: 100px;
		    text-align: right;
		    filter: alpha(opacity=0);
		    opacity: 0;
		    outline: none;
		    background: white;
		    cursor: inherit;
		    display: block;
		}
		#img-upload{
		    width: 100%;
		}
	</style>
@endsection

@section('addjs')	
	{{-- multiselect başladı --}}
	<script src="/js/paraia_multi_select.min.js"></script>
	<script>
		(function() {
			'use strict';
			window.addEventListener('load', function() {
			  var form = document.getElementById('needs-validation');
			  form.addEventListener('submit', function(event) {
			    if (form.checkValidity() === false) {
			      event.preventDefault();
			      event.stopPropagation();
			    }
			    form.classList.add('was-validated');
			  }, false);
			}, false);
		})();
    	// You'd better put the code inside ready() function
	    $(document).ready(function () {
	    	$('#buttonSaveForm').click(function(){
	    		$('#categories').val(select.paraia_multi_select('get_items'));
	    	});
	        // Items to select
	        var items = [
	            {{-- @foreach( $blogcategories as $id => $blogcategory ) 
					{!! '{value: '.$id.", text: '".$blogcategory. "'}," !!}
				@endforeach--}}
	        ];

	        // Initialize paraia-multi-select
	        var select = $('[data-paraia-multi-select="true"]').paraia_multi_select({
	            multi_select: true,
	            items: items,
	            defaults: [{{ old('categories') }}],
	            rtl: true
	        });
	    });
	</script>
	{{-- multiselect bitti --}}

	<script type="text/javascript">
		function slug(str) {
		  str = str.replace(/^\s+|\s+$/g, ''); // trim
		  str = str.toLowerCase();

		  // remove accents, swap ñ for n, etc
		  var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñçşIĞğı·/_,:;";
		  var to   = "aaaaaeeeeeiiiiooooouuuuncsiggi------";
		  for (var i=0, l=from.length ; i<l ; i++) {
		    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		  }

		  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
		    .replace(/\s+/g, '-') // collapse whitespace and replace by -
		    .replace(/-+/g, '-'); // collapse dashes

		  return str;
		};
		$("#title").keyup(function(){
	        $("#url").val( slug( $(this).val() ) );
	    });
		$(document).ready( function() {
	    	$(document).on('change', '.btn-file :file', function() {
			var input = $(this),
				label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [label]);
			});

			$('.btn-file :file').on('fileselect', function(event, label) {
			    
			    var input = $(this).parents('.input-group').find(':text'),
			        log = label;
			    
			    if( input.length ) {
			        input.val(log);
			    } else {
			        if( log ) alert(log);
			    }
		    
			});
			function readURL(input) {
			    if (input.files && input.files[0]) {
			        var reader = new FileReader();
			        
			        reader.onload = function (e) {
			            $('#img-upload').attr('src', e.target.result);
			        }
			        
			        reader.readAsDataURL(input.files[0]);
			    }
			}

			$("#imageFile").change(function(){
			    readURL(this);
			}); 	
		});
	</script>
	<script type="text/javascript" src="/js/tagsinput.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/codemirror.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/xml.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/froala_editor.min.js" ></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/align.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/char_counter.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/code_beautifier.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/code_view.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/colors.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/draggable.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/emoticons.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/entities.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/file.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/font_size.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/font_family.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/fullscreen.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/image.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/image_manager.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/line_breaker.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/inline_style.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/link.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/lists.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/paragraph_format.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/paragraph_style.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/quick_insert.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/quote.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/table.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/save.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/url.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/video.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/help.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/print.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/third_party/spell_checker.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/special_characters.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/word_paste.min.js"></script>
	<script type="text/javascript">
		$(function(){
		  $('#content').froalaEditor()
		});
	</script>
@endsection