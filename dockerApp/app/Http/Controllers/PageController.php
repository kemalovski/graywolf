<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Session;
use App\ImageGallery;
use App\Http\Controllers\UsersLogsController;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();

        return view( 'dashboard.page.index', compact( 'pages' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $imageGalleries = ImageGallery::pluck('title', 'id');

        return view( 'dashboard.page.create', compact('imageGalleries') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UsersLogsController $usersLogs)
    {

        $this->validation($request);                

        $imageName = $request['url'].'.'.request()->imageFile->getClientOriginalExtension();

        $request->merge(['image' => $imageName]);

        request()->imageFile->move(public_path('images/pages'), $imageName);



        if ( $request->image_gallery_id == 0 ) {
        
            $request->request->remove('image_gallery_id');

        }

        Page::create($request->all());

        $usersLogs->store($request, $request['title'].' başlıklı sayfayı kayıt etti.');

        Session::flash('message', 'Sayfa kayıt oldu!');

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {

        $imageGalleries = ImageGallery::pluck('title', 'id');

        return view( 'dashboard.page.edit',compact('page', 'imageGalleries') );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page, UsersLogsController $usersLogs)
    {

        $this->validation($request, $page->id);

        if (request()->imageFile) {

            if( file_exists( public_path('images/pages') . '/' . $page->image ) ){

                unlink( public_path('images/pages') . '/' . $page->image );

            }

             $imageName = $request['url'].'.'.request()->imageFile->getClientOriginalExtension();

            $request->merge(['image' => $imageName]);

            request()->imageFile->move(public_path('images/pages'), $imageName);

        }

        $page->update($request->all());


        $usersLogs->store($request, $page->id.' id\'li sayfa değiştirildi.');

        return redirect()->route('page.edit',$page->id)->with('message','Sayfa başarıyla değiştirildi');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Page $page, UsersLogsController $usersLogs)
    {

        // return $page;

        if( file_exists( public_path('images/pages') . '/' . $page->image ) ){

            unlink( public_path('images/pages') . '/' . $page->image );

        }

        $title = $page->title;

        if ($page->delete()) {

            $usersLogs->store($request, $title.' başlıklı sayfa silindi.');

            return back()->with('message','Sayfa silindi!');
        }

    }

    /**
     * Validation requests.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function validation($request, $id = null)
    {
        return $this->validate( $request, 
            [
                'title' => 'required|max:100|unique:pages,title,'.$id,
                'url' => 'required|max:150|unique:pages,url,'.$id,
                'subtitle' => 'required',
                'content' => 'required',
                'description' => 'required',
                'tags' => 'max:300'
            ]
            ,[
                'title.required' => 'Başlığı boş bırakmayınız.',
                'title.max' => 'Başlığı 100 karakterden fazla girmeyiniz.',
                'title.unique' => 'Bu başlığa ait bir başka sayfa mevcut.',

                'url.required' => 'Uzantıyı boş bırakmayınız.',
                'url.unique' => 'Bu uzantıya ait bir başka sayfa mevcut.',
                'url.max' => 'Uzantı 150 karakterden fazla olamaz.',

                'subtitle.required' => 'Alt başlığı boş bırakmayınız.',
                
                'content.required' => 'İçeriği boş bırakmayınız.',

                'description.required' => 'Google için olan açıklamayı boş bırakmayınız.',
                
                'tags.max' => 'Etiketler 300 karakterden fazla olamaz.'
            ]
        );
    }

}
