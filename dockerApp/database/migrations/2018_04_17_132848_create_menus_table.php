<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('up_id')->default(0);
            $table->integer('order')->default(0);
            $table->string('title', 100);
            $table->mediumText('subtitle');
            $table->string('url', 150)->unique();
            $table->mediumText('description');
            $table->integer('blog_categories_id')->unsigned()->nullable()->unique();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
