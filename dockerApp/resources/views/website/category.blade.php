@extends('website.app')

@section('title')

@endsection


@section('metaDescription'){{$menu->description}}@endsection

@section('addcss')

@endsection

@section('content')

  

      <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading">{{$menu->title}}</h1>
          <p class="lead text-muted">{{$menu->subtitle}}</p>
          {{-- <p>
            <a href="#" class="btn btn-primary my-2">Main call to action</a>
            <a href="#" class="btn btn-secondary my-2">Secondary action</a>
          </p> --}}
        </div>
      </section>

      <div class="album py-5 bg-light">
        <div class="container">

          <div class="row">
            @foreach($menu->BlogCategory->Blog as $blog)
              <div class="col-md-4">
                <a href="/{{$menu->url}}/{{ $blog->url }}">
                  <div class="card mb-4 box-shadow">
                    <img class="card-img-top" src="/images/blogs/{{$blog->image}}" data-holder-rendered="true">
                    <div class="card-body">
                      <p class="card-text">{{$blog->title}}</p>
                      <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                          <button type="button" class="btn btn-sm btn-outline-secondary">Go >> </button>
                        </div>
                        <small class="text-muted">{{ \Carbon\Carbon::parse( $blog->created_at )->diffForHumans( \Carbon\Carbon::now() ) }}</small>
                      </div>
                    </div>
                  </div>
                </a>
              </div>              
            @endforeach
          </div>
        </div>
      </div>

      <hr class="featurette-divider">
@endsection

@section('addjs')

@endsection