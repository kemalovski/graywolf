@extends('dashboard.app')

@section('content')
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Kullanıcılar</h1>
		<p>
		  <a class="btn btn-secondary"  href="{{ route('users.index') }}">
		    Kullanıcıları görüntüle
		  </a>
		  <a class="btn btn-secondary"  href="{{ route('users.create') }}">
		    Kullanıcı Kayıt Et
		  </a>
		</p>
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="dashboard">Kullanıcılar</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Kullanıcı görüntüleme sayfası</li>
		  </ol>
		</nav>
	</div>
	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	@if(Session::has('message'))
	<div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true" style="font-size:20px">×</span>
	  	</button>
	  	{{ Session::get('message') }}
	</div>
	@endif


	{!! Form::open(['method' => 'POST', 'novalidate', 'id' => 'needs-validation', 'action' => ['UserController@update', $user->id] ]) !!}
		
		@method('PUT')

		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">

					{{ Form::label( 'name', 'Ad Soyad', [] ) }}

					{{ Form::text('name', $user->name, ['placeholder' => 'Ad Soyad giriniz.', 'maxlength' => '191', 'class' => 'form-control', 'id' => 'name', 'required'] ) }}

	                <div class="invalid-feedback">
	                	Ad soyadı boş bırakmayınız.
	                </div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">

					{{ Form::label( 'email', 'E-posta', [] ) }}

					{{ Form::text('email', $user->email, ['placeholder' => 'E-posta giriniz.', 'maxlength' => '191', 'class' => 'form-control', 'required'] ) }}

					<div class="invalid-feedback">
	                	E-posta boş bırakmayınız.
	                </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">

					{{ Form::label( 'password', 'Şifre', [] ) }}

					{{ Form::password('password', ['placeholder' => 'Şifrenizi giriniz.', 'class' => 'form-control'] ) }}


					<div class="invalid-feedback">
	                	Şifrenizi boş bırakmayınız.
	                </div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">

					{{ Form::label( 'confirmPassword', 'Şifreyi Eşle', [] ) }}

					{{ Form::password('confirmPassword', ['placeholder' => 'Şifrenizi eşleyiniz.', 'class' => 'form-control'] ) }}

					<div class="invalid-feedback">
	                	Lütfen şifrenizi eşleyiniz
	                </div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">

				{{ Form::submit('Kayıt Et', ['class' => 'btn btn-info col-lg-6', 'id' => 'buttonSaveForm']) }}

			</div>
		</div>	
	{!! Form::close() !!}

@endsection

@section('addcss')
	
<link rel="stylesheet" href="/css/bootstrapValidator.min.css">	
@endsection

@section('addjs')
<script type="text/javascript" src="/js/bootstrapValidator.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
		   	$('#needs-validation').bootstrapValidator({
				message: 'This value is not valid',
				feedbackIcons: {
				    valid: 'glyphicon glyphicon-ok',
				    invalid: 'glyphicon glyphicon-remove',
				    validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
				    name: {
				        message: 'Adınızı soyadınızı düzgün girmediniz.',
				        validators: {
				            notEmpty: {
				                message: 'Ad soyad bize lazım. Lütfen boş bırakmayınız.'
				            },
				            stringLength: {
				                min: 6,
				                max: 100,
				                message: 'Ad Soyad 6 ile 100 karakter arasında olmalıdır.'
				            },
				            regexp: {
				                regexp: /^[a-zA-ZğüşöçİĞÜŞıÖÇ ]+$/,
				                message: 'Ad Soyad alfabe karakterlerinden başka karakterler kullanılmamalı.'
				            }
				        }
				    },
				    email: {
				        validators: {
				            notEmpty: {
				                message: 'E-posta bize lazım. Lütfen boş bırakmayınız'
				            },
				            emailAddress: {
				                message: 'E-posta adresi bir e-posta değildir.'
				            }
				        }
				    },
				    
				    password: {
				        validators: {
				            
				            identical: {
				                field: 'confirmPassword',
				                message: 'Şifre eşleme alanı ile şifre birbiri ile uyuşmuyor.'
				            }
				        }
				    },
				    confirmPassword: {
				        validators: {
				            
				            identical: {
				                field: 'password',
				                message: 'Şifre ve eşleme alanı eşleşmiyor.'
				            }
				        }
				    }
				}
			});
		});

	    
	</script>
	
@endsection