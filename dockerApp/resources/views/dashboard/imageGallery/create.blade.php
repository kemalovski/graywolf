@extends('dashboard.app')



@section('addcss')
    <link rel="stylesheet" href="/css/dropzone.css">
    <link rel="stylesheet" href="/css/tagsinput.css">
@endsection



@section('addjs')
	<script type="text/javascript" src="/js/tagsinput.js"></script>
	<script src="/js/dropzone.js"></script>
  	<script src="/js/forms_file-upload.js"></script>
  	<script type="text/javascript">
  		function slug(str) {
		  str = str.replace(/^\s+|\s+$/g, ''); // trim
		  str = str.toLowerCase();

		  // remove accents, swap ñ for n, etc
		  var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñçşIĞğı·/_,:;";
		  var to   = "aaaaaeeeeeiiiiooooouuuuncsiggi------";
		  for (var i=0, l=from.length ; i<l ; i++) {
		    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		  }

		  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
		    .replace(/\s+/g, '-') // collapse whitespace and replace by -
		    .replace(/-+/g, '-'); // collapse dashes

		  return str;
		};
		$("#title").keyup(function(){
	        $("#url").val( slug( $(this).val() ) );
	    });
	    (function() {
			'use strict';
			window.addEventListener('load', function() {
			  var form = document.getElementById('needs-validation');
			  form.addEventListener('submit', function(event) {
			    if (form.checkValidity() === false) {
			      event.preventDefault();
			      event.stopPropagation();
			    }
			    form.classList.add('was-validated');
			  }, false);
			}, false);
		})();
    	// You'd better put the code inside ready() function
  	</script>
@endsection




@section('content')
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Resim Galerisi</h1>
		<p>
		  <a class="btn btn-secondary"  href="{{ route('imagegallery.index') }}">
		    Resim Galerilerini Görüntüle
		  </a>
		  <a class="btn btn-secondary disabled"  href="{{ route('imagegallery.create') }}">
		    Resim Galerisi Kayıt Et
		  </a>
		</p>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ route('imagegallery.index') }}">Resim Galerileri</a></li>
		    	<li class="breadcrumb-item active" aria-current="page">Resim Galerileri Formu</li>
			</ol>
		</nav>
	</div>
	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	@if(Session::has('message'))
		<div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    	<span aria-hidden="true" style="font-size:20px">×</span>
		  	</button>
		  	{{ Session::get('message') }}
		</div>
	@endif

	<div class="row">
		<div class="col-lg-12">
			{!! Form::open(['method' => 'POST', 'novalidate', 'id' => 'needs-validation', 'action' => 'ImageGalleryController@store']) !!}
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							{{ Form::label( 'title', 'Başlık', [] ) }}
							{{ Form::text('title', old('title'), ['placeholder' => 'Galeri adını giriniz.', 'maxlength' => '200', 'class' => 'form-control', 'id' => 'title', 'required'] ) }}
							<div class="invalid-feedback">
			                	Başlığı boş bırakmayınız.
			                </div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							{{ Form::label( 'url', 'Uzantı', [] ) }}
							{{ Form::text('url', old('url'), ['placeholder' => 'Uzantı kendiliğinden oluşacaktır.', 'maxlength' => '150', 'class' => 'form-control', 'id' => 'url', 'readonly', 'required'] ) }}
							<div class="invalid-feedback">
			                	Uzantıyı boş bırakmayınız.
			                </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							{{ Form::label( 'subtitle', 'Alt Başlık', [] ) }}
							{{ Form::text('subtitle', old('subtitle'), ['placeholder' => 'Galeri alt başlığını giriniz.', 'maxlength' => '200', 'class' => 'form-control', 'id' => 'subtitle'] ) }}
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							{{ Form::label( 'description', 'Google için Açıklama', [] ) }}
							{{ Form::text('description', old('description'), ['placeholder' => 'Açıklama giriniz.', 'class' => 'form-control', 'id' => 'description'] ) }}
							<div class="invalid-feedback">
			                	Açıklamayı boş bırakmayınız.
			                </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							{{ Form::label( 'tags', 'Etiketler' ) }}
							{{ Form::text('tags', old('tags'), ['placeholder' => 'Etiket giriniz.', 'class' => 'form-control', 'maxlength' => '200', 'id' => 'tags', 'data-role' => 'tagsinput'] ) }}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						{{ Form::submit('Kayıt Et', ['class' => 'btn btn-info col-lg-12', 'id' => 'buttonSaveForm']) }}
					</div>
				</div>
			{!! Form::close() !!}
			
		</div>
	</div>
	

	
@endsection

{{-- 

<!DOCTYPE html>

<html lang="en" class="default-style">

<head>
  <title>File upload - Forms - Appwork</title>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="/js/dropzone.js"></script>
  	<script src="/js/forms_file-upload.js"></script>

  <link rel="stylesheet" type="text/css" href="/css/dropzone.css">
</head>

<body>
  
<form action="/upload" class="dropzone needsclick" id="dropzone-demo">
  <div class="dz-message needsclick">
    Drop files here or click to upload
    <span class="note needsclick">(This is just a demo dropzone. Selected files are
      <strong>not</strong> actually uploaded.)</span>
  </div>
  <div class="fallback">
    <input name="file" type="file" multiple>
  </div>
</form>

</body>

</html> --}}