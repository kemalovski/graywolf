<?php 

function generate_menu( $parent_id = 0 ){

	$menus = '';

	$row = DB::table('menus')->where('up_id', $parent_id)->orderBy('order')->get();

	for ($i=0; $i < count($row); $i++) {
		#eğer bir blog kategorsine ya da sayfaya bağlı değilse blog sitelerinde gösterme
		if (isset($row[$i]->pages_id) or isset($row[$i]->blog_categories_id)) {
			
			if (isset($row[$i]->pages_id)) {
				$url = 'sayfa/'.$row[$i]->url;
			}else{
				$url = $row[$i]->url;	
			}
			if ( $row[$i]->has_children == 0 && $row[$i]->up_id == 0 ) {

				$menus .= '<li class="nav-item';
				if (Request::segment(1) == $row[$i]->url) {
					$menus .= ' active';
				}else if(Request::segment(2) == $row[$i]->url){
					$menus .= ' active';
				}
				$menus .= '">   <a class="nav-link" href="/'.$url.'">'.$row[$i]->title.'</a> </li>';
				generate_menu( $row[$i]->id );

			}elseif ( $row[$i]->has_children == 1 && $row[$i]->up_id == 0 ) {

				$menus .= '<li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="/'.$url.'" > '.$row[$i]->title.' </a> <ul class="dropdown-menu">'.generate_menu( $row[$i]->id ).'</ul></li>';

			}elseif ( $row[$i]->has_children == 0 && $row[$i]->up_id != 0 ) {

				$menus .= '<li><a class="dropdown-item" href="/'.$url.'">'.$row[$i]->title.'</a></li>';
				generate_menu( $row[$i]->id );

			}elseif ($row[$i]->has_children == 1 && $row[$i]->up_id != 0) {

				$menus .= '<li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" data-toggle="dropdown" href="/'.$url.'">'.$row[$i]->title.'</a> <ul class="dropdown-menu">'.generate_menu( $row[$i]->id ).'</ul></li>';
			}
		}else{
			generate_menu( $row[$i]->id );
		}

	}

	$menus .= '';

	return $menus;
}

function generate_menu_personalWebSite( $parent_id = 0 ){

	$menus = '';

	// menuleri order sırasına göre çekiyoruz.
	$row = DB::table('menus')->where('up_id', $parent_id)->orderBy('order')->get();
	// menüleri saydırıyoruz
	for ($i=0; $i < count($row); $i++) {
		// sayfaya bağlı ise menünün urlsini ona göre yapıyoruz
		if (isset($row[$i]->pages_id)) {
			$url = 'sayfa/'.$row[$i]->url;
		}elseif (isset($row[$i]->blog_categories_id)) {
			$url = '/'.$row[$i]->url;
		}else{
			$url = '#'.$row[$i]->url;
		}
		
		$menus .= '<li>   <a href="'.$url.'">'.$row[$i]->title.'</a> </li>';
		generate_menu_personalWebSite( $row[$i]->id );

	}

	$menus .= '';

	return $menus;
}

function getMenuJson(){

	$menus = '<ol class="sortable ui-sortable mjs-nestedSortable-branch mjs-nestedSortable-expanded">';

	$menus .= generate_multilevel_Json(0, 'menus', '/graywolf/menu/edit/');

	$menus .= '</ol>';

	return $menus;
}

function getBlogCategoryJson(){

	$menus = '<ol class="sortable ui-sortable mjs-nestedSortable-branch mjs-nestedSortable-expanded">';

	$menus .= generate_multilevel_Json(0, 'blog_categories', '/graywolf/blog/kategoriler/edit/');

	$menus .= '</ol>';

	return $menus;
}

function generate_multilevel_Json( $parent_id = 0, $table, $editUrl){

	$menu = '';

	$row = DB::table($table)->where('up_id', $parent_id)->orderBy('order')->get();

	for ($i=0; $i < count($row); $i++) { 

		$menu .= '<li style="display: list-item;" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded" id="menuItem_'.$row[$i]->id.'" data-foo="bar">';

		$menu .= '<div class="menuDiv">

				   <span title="Click to show/hide children" class="disclose ui-icon ui-icon-minusthick"><span></span></span>

				   <span><a href="'.$editUrl.$row[$i]->id.'"><span data-id="'.$row[$i]->id.'" class="itemTitle">'.$row[$i]->id.'-'.$row[$i]->title.'</span></a></span>
				   	<span title="Click to delete item." data-id="'.$row[$i]->id.'" data-toggle="modal" onclick="removeButtonClicked('.$row[$i]->id.')" data-target="#removeMenuModal" class="deleteMenu ui-icon ui-icon-closethick"><span></span></span>
				   	<a href="'.$editUrl.$row[$i]->id.'"><span title="Click to edit item." data-id="'.$row[$i]->id.'" class="editmenu ui-icon ui-icon-pencil"><span></span></span></a>
				   </span>
				   
			    </div>';

		$menu .= '<ol>'.generate_multilevel_Json($row[$i]->id, $table, $editUrl).'</ol>';

	}

	return $menu;
}

