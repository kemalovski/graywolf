<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redis;
use App\Menu;
use App\BlogCategory;
use App\Page;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\UsersLogsController;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.menu.index', 
            [
                'menus' => Menu::orderBy('created_at','asc')->get(), 
                'blogCategories' => BlogCategory::orderBy('created_at','asc')->get(),
                'pages' => Page::orderBy('created_at','asc')->get()->pluck('id','title')
            ] 
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function sorting(Request $request, UsersLogsController $usersLogs)
    {

        // ilk gelen sorting arrayinin ilk elemanı boş olduğundan onu siliyoruz
        $sorting = json_decode($request['sorting']);

        $usersLogs->store($request, 'menülerin yerini değiştirdi.');

        unset($sorting[0]);

        // arrayin elimize gelme sırasını oluşturmak için bu değişkeni yaratıyoruz.
        $order = 0;
        
        foreach ($sorting as $key => $sort) {

            $order ++;

            if ($sort->parent_id == null) {

                $sort->parent_id = 0;

            }

            $menu = Menu::find($sort->id);

            // alt elemanları var mı kontrol ediyoruz bazı html değişkenleri için gerekli oluyor.
            if ( array_search( $sort->id, array_column($sorting, 'parent_id') ) ) {

                $menu->has_children = 1;

            }else{

                $menu->has_children = 0;

            }

            $menu->up_id = $sort->parent_id;

            $menu->order = $order;

            $menu->save();

        }

        return json_encode($sorting);

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UsersLogsController $usersLogs)
    {

        $this->validation($request);

        if (Menu::create($request->all())) {
            
            Session::flash('message', 'Menü kayıt oldu!');

            $usersLogs->store($request, $request['title'].' başlıklı menüyü kayıt etti.');

            return back();
        }
        
        return 'olmadı be gülüm';
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function showEditForm($id)
    {

        return view( 'dashboard.menu.showEditForm',
            [   
                'menu' => Menu::find($id),
                'menus' => Menu::orderBy('created_at','asc')->get(),
                'blogCategories' => BlogCategory::orderBy('created_at','asc')->get(),
                'pages' => Page::orderBy('created_at','asc')->get()->pluck('id','title')
            ]  
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function editMenu($id, Request $request, UsersLogsController $usersLogs)
    {
        
        $this->validation($request, $id);

        $menu = Menu::find($id);

        $usersLogs->store($request, $menu->title.' başlıklı menüyü değiştirdi.');

        $menu->title = $request->get('title');
        $menu->pages_id = $request->get('pages_id');
        $menu->subtitle = $request->get('subtitle');
        $menu->url = $request->get('url');
        $menu->description = $request->get('description');
        $menu->up_id = $request->get('up_id');
        $menu->blog_categories_id = $request->get('blog_categories_id');

        $menu->save();

        Session::flash('message', 'Menü değiştirildi!');

        return redirect()->route('menu.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, UsersLogsController $usersLogs)
    {
        if(isset($request['id'])){

            $menu = Menu::find($request['id']);

            $usersLogs->store($request,$menu->title.' başlıklı menüyü sildi.');

            $menu->delete();

            Session::flash('message', $request['id'].' id\' li menü silindi.');

            return redirect()->action('MenuController@index');
            
        }
    }

    public function validation($request, $id = null)
    {
        return $this->validate( $request, 
            [
                'title' => 'required|max:100',
                'url' => 'required|max:150|unique:menus,url,'.$id,
                'pages_id' => 'nullable|unique:menus,pages_id,'.$id,
                'blog_categories_id' => 'nullable|unique:menus,blog_categories_id,'.$id,
            ]
            ,[
                'blog_categories_id.unique' => 'Bu kategorinin başka bir menüsü mevcuttur.',
                'title.required' => 'Başlığı boş bırakmayınız.',
                'pages_id.unique' => 'Bu sayfanın başka bir menüsü mevcuttur.',
                'title.max' => 'Başlığı 100 karakterden fazla girmeyiniz.',
                'url.required' => 'Uzantı boş bırakmayınız.',
                'url.unique' => 'Bu uzantıya ait bir başka kategori mevcut.',
                'url.max' => 'Uzantı 150 karakterden fazla olamaz.',
            ]
        );
    }



    public function showArticle($id){

        if( Redis::zScore('articleViews', 'article:' . $id) ){

            Redis::pipeline(function($pipe) use ($id){

                $pipe->zIncrBy('articleViews', 1, 'article' . $id );
                $pipe->incr('article:' . $id . ':views');
                // zRange articleViews 0 -1 bunu cli da yazıp görecen    

            });
            
        }
        else{

            $views = Redis::incr('article:' . $id . ':views');
            Redis::zIncrBy('articleViews', $views, 'article:' . $id);

        }

        $views = Redis::get('article:' . $id . ':views');

        return "This is an article with id: " . $id . " it has " . $views . " views";

    }


    public function __construct(Menu $menu){
        
        $this->menu = $menu;

    }


    public function showBlog(){

        DB::connection()->enableQueryLog();

        $menus = $this->menu->fetchAll();

        $log = DB::getQueryLog();

        print_r($log);

    }


}


