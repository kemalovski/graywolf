<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'title', 'subtitle', 'url', 'description', 'content', 'tags', 'image_gallery_id', 'image'
    ];

    public function ImageGallery()
	{
		return $this->belongsTo('App\ImageGallery');
	}
}
