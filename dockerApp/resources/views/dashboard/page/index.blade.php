@extends('dashboard.app')

@section('content')
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Sayfa</h1>
		<p>
		  <a class="btn btn-secondary disabled"  href="{{ route('page.index') }}">
		    Sayfaları görüntüle
		  </a>
		  <a class="btn btn-secondary"  href="{{ route('page.create') }}">
		    Sayfa Kayıt Et
		  </a>
		</p>
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="dashboard">Sayfa</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Kayıt etme sayfası</li>
		  </ol>
		</nav>
	</div>
	@if(Session::has('message'))
		<div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    	<span aria-hidden="true" style="font-size:20px">×</span>
		  	</button>
		  	{{ Session::get('message') }}
		</div>
	@endif
	<table id="datatables" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Başlık</th>
                <th>Açıklama</th>
                <th>Oluşturulma Tarihi</th>
                <th>İşlemler</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($pages as $page)

                <tr>
                    <td>{{ $page->id }}</td>
                    <td>{{ $page->title }}</td>
                    <td>{{ $page->description }}</td>
                    <td>{{ $page->created_at }}</td>
                    <td>
                    	<span class="badge badge-pill badge-danger col-md-3" onclick="removeButtonClicked({{ $page->id }})" data-toggle="modal" data-target="#removeBlogModal">Sil</span>
                    	<span class="badge badge-pill badge-warning col-md-3"><a href="{{ route('page.edit',$page->id) }}">Değiştir</a></span>
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Id</th>
                <th>Başlık</th>
                <th>Açıklama</th>
                <th>Oluşturulma Tarihi</th>
                <th>İşlemler</th>
            </tr>
        </tfoot>
    </table>
	<div class="modal fade" id="removeBlogModal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLongTitle">Sayfa Silme İşlemi</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Evete tıklarsanız, sayfayı silmiş olacaksınız. Emin misiniz?
	        <input type="hidden" id="pageId" >
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Hayır</button>
	        <button type="button" class="btn btn-primary" id="removeBlog" >Evet</button>
	      </div>
	    </div>
	  </div>
	</div>

@endsection

@section('addcss')
	<link rel="stylesheet" href="/css/dataTables.bootstrap4.min.css">
	<style type="text/css">
		.badge-danger{
			cursor: pointer;
		}
	</style>
@endsection

@section('addjs')
	<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript">
		function removeButtonClicked(pageId){
			$('#pageId').val(pageId);
		}
		function postAndRedirect(url, postData)
		{
		    var postFormStr = "<form method='POST' action='" + url + "'>\n";
		    for (var key in postData)
		    {
		        if (postData.hasOwnProperty(key))
		        {
		            postFormStr += '<input type="hidden" name="id" value="' + postData + '"></input>'+'@csrf'+'@method("DELETE")';
		        }
		    }
		    postFormStr += "</form>";

		    var formElement = $(postFormStr);

		    $('body').append(formElement);
		    $(formElement).submit();
		}
		$(document).ready(function() {
			$('#removeBlog').click(function(){
				$('#removeBlogModal').modal('hide');
				postAndRedirect( '/graywolf/page/'+$('#pageId').val(), $('#pageId').val() );
			});
		    $('#datatables').DataTable({
		    	"order": [[ 3, "desc" ]]
		    });
		});
	</script>
	
@endsection

