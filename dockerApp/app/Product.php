<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'title', 'subtitle', 'url', 'description', 'image', 'content', 'categories', 'tags', 'image_gallery_id'
    ];

    public function ImageGallery()
	{
		return $this->belongsTo('App\ImageGallery');
	}
    
}
