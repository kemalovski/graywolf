<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Menu extends Model
{

    protected $fillable = [
        'up_id', 'title', 'url', 'blog_categories_id', 'pages_id'
    ];

    public function fetchAll(){

    	$result = Cache::remember('menus', 1, function () {
			return DB::table('menus')->get();
		});

    	return $result;

    }

    public function fetch(){
    	
    	$result = Cache::remember('menus', 1, function () {
			return DB::table('menus')->get();
		});

    	return $result;

    }

    public function BlogCategory(){
    	return $this->belongsTo('App\BlogCategory', 'blog_categories_id', 'id');
    }

    public function Page(){
    	return $this->belongsTo('App\Page', 'pages_id', 'id');
    }

}
