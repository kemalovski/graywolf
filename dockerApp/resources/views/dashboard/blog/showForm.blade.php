@extends('dashboard.app')

@section('content')
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Blog</h1>
		<p>
		  <a class="btn btn-secondary"  href="{{ route('blog.view') }}">
		    Blogları görüntüle
		  </a>
		  <a class="btn btn-secondary disabled"  href="{{ route('blog.showForm') }}">
		    Blog Kayıt Et
		  </a>
		  <a class="btn btn-secondary"  href="{{ route('blog.categoryIndex') }}">
		    Blog Kategorileri
		  </a>
		</p>
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="dashboard">Blog</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Kayıt etme sayfası</li>
		  </ol>
		</nav>

	</div>
	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	@if(Session::has('message'))
	<div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true" style="font-size:20px">×</span>
	  	</button>
	  	{{ Session::get('message') }}
	</div>
	@endif

	
	<form id="needs-validation" novalidate method="post" action="/graywolf/blog/save" enctype="multipart/form-data">
		@csrf
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="title">Başlık</label>
					<input type="text" maxlength="100" value="{{ old('title') }}" class="form-control" id="title" name="title" placeholder="Başlık giriniz" required>
	                <div class="invalid-feedback">
	                	Başlığı boş bırakmayınız.
	                </div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="url">Uzantı</label>
					<input type="text" maxlength="150" value="{{ old('url') }}" class="form-control" name="url" id="url" placeholder="Uzantı kendiliğinden oluşacaktır" readonly>
					<div class="invalid-feedback">
	                	Uzantıyı boş bırakmayınız.
	                </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="subtitle">Alt Başlık</label>
					<input type="text" class="form-control" value="{{ old('subtitle') }}" name="subtitle" id="subtitle" placeholder="Alt başlık giriniz." required>
					<div class="invalid-feedback">
	                	Alt başlığı boş bırakmayınız.
	                </div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="categories">Kategoriler</label>
					<input type="text" maxlength="50" class="form-control" name="categories" id="categories" data-paraia-multi-select="true" placeholder="Kategori Seçiniz" required>

					<div class="invalid-feedback">
		            	Kategori seçiniz.
		            </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
			        <label for="imageFile">Resim</label>
			        <div class="input-group">
			            <span class="input-group-prepend">
			                <span class="btn btn-outline-secondary btn-file">
			                    Resim seçiniz <input class="form-control" type="file" id="imageFile" name="imageFile" required>
			                </span>
			            </span>
			            <input type="text" class="form-control" name="imgInp" id="imgInp" required>
			            <div class="invalid-feedback">
		                	Resmi boş bırakmayınız
		                </div>
			        </div>
			        <img id='img-upload'  />
			    </div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="image_gallery_id">Resim Galerisi</label>
					<select class="form-control" id="image_gallery_id" name="image_gallery_id">
				      <option value="">Resim Galerisini seçebilirsiniz</option>
				      @foreach( $imageGalleries as $key => $imageGallery )
				      	<option value="{{$key}}">{{$imageGallery}}</option>
				      @endforeach
				    </select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<label for="content">İçerik</label>
					<div id="editor">
						<textarea name="content" id="content" class="form-control" required>{{ old('content') }}</textarea>
						<div class="invalid-feedback">
		                	İçeriksiz internet sayfası mı olurmuş?
		                </div>
					</div>
			  	</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="description">Google için Açıklama</label>
					<input type="text" class="form-control" value="{{ old('description') }}" name="description" id="description" placeholder="Açıklama giriniz" required>
					<div class="invalid-feedback">
	                	Açıklamayı boş bırakmayınız.
	                </div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="tags">Etiketler</label>
					<input type="text" class="form-control" maxlength="300" value="{{ old('tags') }}" id="tags" name="tags" data-role="tagsinput" placeholder="Etiket giriniz">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<button type="submit" class="btn btn-info col-lg-6" id="buttonSaveForm">Kaydet</button>
			</div>
		</div>	
	</form>
@endsection

@section('addcss')

	{{-- multiselect başladı --}}
    <link rel="stylesheet" href="/css/paraia_multi_select.min.css">

	{{-- multiselect bitti --}}

	<link rel="stylesheet" href="/css/tagsinput.css">
	
	<link rel="stylesheet" href="/css/richTextEditor/froala_editor.css">
	<link rel="stylesheet" href="/css/richTextEditor/froala_style.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/code_view.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/draggable.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/colors.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/emoticons.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/image_manager.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/image.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/line_breaker.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/table.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/char_counter.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/video.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/fullscreen.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/file.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/quick_insert.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/help.css">
	<link rel="stylesheet" href="/css/richTextEditor/third_party/spell_checker.css">
	<link rel="stylesheet" href="/css/richTextEditor/plugins/special_characters.css">
	<link rel="stylesheet" href="/css/richTextEditor/codemirror.min.css">
	
	<style type="text/css">
		.btn-file {
		    position: relative;
		    overflow: hidden;
		}
		.btn-file input[type=file] {
		    position: absolute;
		    top: 0;
		    right: 0;
		    min-width: 100%;
		    min-height: 100%;
		    font-size: 100px;
		    text-align: right;
		    filter: alpha(opacity=0);
		    opacity: 0;
		    outline: none;
		    background: white;
		    cursor: inherit;
		    display: block;
		}
		#img-upload{
		    width: 100%;
		}
	</style>
@endsection

@section('addjs')	
	{{-- multiselect başladı --}}
	<script src="/js/paraia_multi_select.min.js"></script>
	<script>
		(function() {
			'use strict';
			window.addEventListener('load', function() {
			  var form = document.getElementById('needs-validation');
			  form.addEventListener('submit', function(event) {
			    if (form.checkValidity() === false) {
			      event.preventDefault();
			      event.stopPropagation();
			    }
			    form.classList.add('was-validated');
			  }, false);
			}, false);
		})();
    	// You'd better put the code inside ready() function
	    $(document).ready(function () {
	    	$('#buttonSaveForm').click(function(){
	    		$('#categories').val(select.paraia_multi_select('get_items'));
	    	});
	        // Items to select
	        var items = [
	            @foreach( $blogcategories as $id => $blogcategory )
					{!! '{value: '.$id.", text: '".$blogcategory. "'}," !!}
				@endforeach
	        ];

	        // Initialize paraia-multi-select
	        var select = $('[data-paraia-multi-select="true"]').paraia_multi_select({
	            multi_select: true,
	            items: items,
	            defaults: [{{ old('categories') }}],
	            rtl: true
	        });
	    });
	</script>
	{{-- multiselect bitti --}}

	<script type="text/javascript">
		function slug(str) {
		  str = str.replace(/^\s+|\s+$/g, ''); // trim
		  str = str.toLowerCase();

		  // remove accents, swap ñ for n, etc
		  var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñçşIĞğı·/_,:;";
		  var to   = "aaaaaeeeeeiiiiooooouuuuncsiggi------";
		  for (var i=0, l=from.length ; i<l ; i++) {
		    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		  }

		  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
		    .replace(/\s+/g, '-') // collapse whitespace and replace by -
		    .replace(/-+/g, '-'); // collapse dashes

		  return str;
		};
		$("#title").keyup(function(){
	        $("#url").val( slug( $(this).val() ) );
	    });
		$(document).ready( function() {
	    	$(document).on('change', '.btn-file :file', function() {
			var input = $(this),
				label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [label]);
			});

			$('.btn-file :file').on('fileselect', function(event, label) {
			    
			    var input = $(this).parents('.input-group').find(':text'),
			        log = label;
			    
			    if( input.length ) {
			        input.val(log);
			    } else {
			        if( log ) alert(log);
			    }
		    
			});
			function readURL(input) {
			    if (input.files && input.files[0]) {
			        var reader = new FileReader();
			        
			        reader.onload = function (e) {
			            $('#img-upload').attr('src', e.target.result);
			        }
			        
			        reader.readAsDataURL(input.files[0]);
			    }
			}

			$("#imageFile").change(function(){
			    readURL(this);
			}); 	
		});
	</script>
	<script type="text/javascript" src="/js/tagsinput.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/codemirror.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/xml.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/froala_editor.min.js" ></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/align.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/char_counter.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/code_beautifier.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/code_view.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/colors.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/draggable.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/emoticons.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/entities.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/file.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/font_size.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/font_family.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/fullscreen.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/image.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/image_manager.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/line_breaker.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/inline_style.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/link.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/lists.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/paragraph_format.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/paragraph_style.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/quick_insert.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/quote.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/table.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/save.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/url.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/video.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/help.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/print.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/third_party/spell_checker.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/special_characters.min.js"></script>
	<script type="text/javascript" src="/js/richTextEditor/plugins/word_paste.min.js"></script>
	<script type="text/javascript">
		$(function(){
		  $('#content').froalaEditor()
		});
	</script>
@endsection