@extends('dashboard.app')

@section('content')
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Blog</h1>
		<p>
		  <a class="btn btn-secondary"  href="{{ route('blog.view') }}">
		    Blogları görüntüle
		  </a>
		  <a class="btn btn-secondary"  href="{{ route('blog.showForm') }}">
		    Blog Kayıt Et
		  </a>
		  <a class="btn btn-secondary"  href="{{ route('blog.categoryIndex') }}">
		    Blog Kategorileri
		  </a>
		</p>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="dashboard">Blog</a></li>
		    	<li class="breadcrumb-item active" aria-current="page">Blog Kategorileri Sayfası</li>
			</ol>
		</nav>
	</div>
	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	@if(Session::has('message'))
		<div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    	<span aria-hidden="true" style="font-size:20px">×</span>
		  	</button>
		  	{{ Session::get('message') }}
		</div>
	@endif

	<div class="row">
		<div class="col-lg-6">
			<form id="needs-validation" novalidate method="post" enctype="multipart/form-data">
				@csrf
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="title">Kategori Adı</label>
							<input type="text" maxlength="100" value="{{ old('title') }}{{ $blogCategory->title }}" class="form-control" id="title" name="title" placeholder="Kategori adını giriniz." required>
			                <div class="invalid-feedback">
			                	Kategori adını boş bırakmayınız.
			                </div>
						</div>
					</div>
				</div>
				<input type="hidden" maxlength="150" value="{{ old('url') }}{{ $blogCategory->url }}" class="form-control" name="url" id="url" required>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="title">Alt Başlık</label>
							<input type="text" value="{{ old('subtitle') }}{{ $blogCategory->subtitle }}" class="form-control" id="subtitle" name="subtitle" placeholder="Alt başlık giriniz." required>
			                <div class="invalid-feedback">
			                	Alt Başlığı boş bırakmayınız.
			                </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="description">Google için Açıklama</label>
							<textarea class="form-control" id="description" name="description" placeholder="Açıklama giriniz." rows="3" required>{{ old('description') }}{{ $blogCategory->description }}</textarea>
							
							<div class="invalid-feedback">
			                	Açıklamayı boş bırakmayınız.
			                </div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
					        <label for="imageFile">Resim</label>
					        <div class="input-group">
					            <span class="input-group-prepend">
					                <span class="btn btn-outline-secondary btn-file">
					                    Resim seçiniz <input class="form-control" type="file" id="imageFile" name="imageFile" >
					                </span>
					            </span>
					            <input type="text" class="form-control" value="{{ $blogCategory->image }}" name="imgInp" id="imgInp" >
					            <div class="invalid-feedback">
				                	Resmi boş bırakmayınız
				                </div>
					        </div>
					        <img id='img-upload' src="{{ url('/') }}/images/blogCategories/{{ $blogCategory->image }}" />
					    </div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<button type="submit" class="btn btn-info col-lg-12" id="buttonSaveForm">Kaydet</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-6">
			{!! getBlogCategoryJson() !!}
		</div>
	</div>
	
	<div class="modal fade" id="removeMenuModal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLongTitle">Blog Kategori Silme İşlemi</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Evete tıklarsanız, Kategoriyi silmiş olacaksınız. Emin misiniz?
	        <input type="hidden" id="blogCategoryId" >
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="hayir">Hayır</button>
	        <button type="button" class="btn btn-primary" id="removeMenu" >Evet</button>
	      </div>
	    </div>
	  </div>
	</div>

	
@endsection

@section('addcss')
	<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/css/nestedSortable.css">
	<style type="text/css">
		.btn-file {
		    position: relative;
		    overflow: hidden;
		}
		.btn-file input[type=file] {
		    position: absolute;
		    top: 0;
		    right: 0;
		    min-width: 100%;
		    min-height: 100%;
		    font-size: 100px;
		    text-align: right;
		    filter: alpha(opacity=0);
		    opacity: 0;
		    outline: none;
		    background: white;
		    cursor: inherit;
		    display: block;
		}
		#img-upload{
		    width: 100%;
		}
	</style>
@endsection

@section('addjs')
	<script src="/js/jquery.min.js"></script>
	<script src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/jquery.mjs.nestedSortable.js"></script>
	<script type="text/javascript" src="/js/blogCategory.js"></script>
	<script type="text/javascript">


	//image upload
	$(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {
		    
		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;
		    
		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }
	    
		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        
		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }
		        
		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imageFile").change(function(){
		    readURL(this);
		}); 	
		$("#title").keyup(function(){
	        $("#url").val( slug( $(this).val() ) );
	    });
	});
	function postAndRedirect(url, postData)
	{
	    var postFormStr = "<form method='POST' action='" + url + "'>\n";
	    for (var key in postData)
	    {
	        if (postData.hasOwnProperty(key))
	        {
	            postFormStr += '@csrf'+"<input type='hidden' name='id' value='" + postData + "'></input>";
	        }
	    }
	    postFormStr += "</form>";

	    var formElement = $(postFormStr);

	    $('body').append(formElement);
	    $(formElement).submit();
	}
	function slug(str) {
		str = str.replace(/^\s+|\s+$/g, ''); // trim
		str = str.toLowerCase();

		// remove accents, swap ñ for n, etc
		var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñçşIĞğı·/_,:;";
		var to   = "aaaaaeeeeeiiiiooooouuuuncsiggi------";
		for (var i=0, l=from.length ; i<l ; i++) {
		str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		}

		str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
		.replace(/\s+/g, '-') // collapse whitespace and replace by -
		.replace(/-+/g, '-'); // collapse dashes

		return str;
	}
	</script>
@endsection

































































