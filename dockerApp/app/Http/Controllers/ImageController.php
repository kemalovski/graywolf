<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use App\ImageGallery;
use Session;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image as CreateImage;
use App\Http\Controllers\UsersLogsController;
// use App\Upload;

class ImageController extends Controller
{
    private $photos_path;
 
    public function __construct()
    {
        $this->photos_path = public_path('/resim_galerileri');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UsersLogsController $usersLogs)
    {

        $photos = $request->file('file');
    
        if (!is_array($photos)) {
            $photos = [$photos];
        }
        
        $imageGallery = ImageGallery::find($request->image_gallery_id);

        

        if ( !is_dir($this->photos_path .'/'. $imageGallery->url) ) {
            mkdir($this->photos_path .'/'. $imageGallery->url, 0755);
        }
        
        for ($i = 0; $i < count($photos); $i++) {

            $photo = $photos[$i];
            $name = sha1(date('YmdHis') . str_random(30));
            $save_name = $name . '.' . $photo->getClientOriginalExtension();
            $resize_name = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();
 
            CreateImage::make($photo)
                ->resize(250, null, function ($constraints) {
                    $constraints->aspectRatio();
                })
                ->save($this->photos_path . '/' . $imageGallery->url . '/' . $resize_name);
 
            $photo->move($this->photos_path . '/' . $imageGallery->url , $save_name);
 
            $Image = new Image();
            $Image->filename = $save_name;
            $Image->resized_name = $resize_name;
            $Image->original_name = basename($photo->getClientOriginalName());
            $Image->url = $imageGallery->url;
            $Image->image_gallery_id = $request->image_gallery_id;
            if($Image->save()){
                $usersLogs->store($request, $imageGallery->title.' başlıklı resim galerisine '.$Image->original_name.' isimli resim kayıt etti.');
            }
        }
        return Response::json([
            'message' => 'Image saved Successfully'
        ], 200);   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    private function getImageProperties($images, $galery_url){

        $path = $this->photos_path.'/'.$galery_url;

        $imagesProperties = array();
        
        $i = 0;

        foreach ($images as $value) {
            
            $realfile = $path . "/" . $value['resized_name'];

            $image['name'] = $value['resized_name'];
            
            $image['size'] = filesize($realfile);

            $imagesProperties[] = $image;
        }

        return json_encode($imagesProperties);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit($image_gallery_id) //Image $image
    {

        if( $imageGallery = ImageGallery::find($image_gallery_id) ){

            $images = Image::select('resized_name')->where('image_gallery_id', $image_gallery_id)->get();

            $images = $this->getImageProperties($images, $imageGallery['url']);

            $imageGalleryUrl = $imageGallery;

            Session::flash('message', $imageGallery->title.' Galerisinin resimlerini girin.');

            return view( 'dashboard.imageGallery.images.edit', compact( 'image_gallery_id', 'images', 'imageGalleryUrl' ) );

        }else{

            return abort(404);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, UsersLogsController $usersLogs)
    {
        
        $filename = $request->id;

        $uploaded_image = Image::where('resized_name', basename($filename))
        ->orWhere('original_name', basename($filename))->first();
 
        if (empty($uploaded_image)) {

            return Response::json(['message' => 'Sorry file does not exist', 'Request' => $request->id], 400);

        }
 
        $file_path = $this->photos_path . '/' . $uploaded_image->url . '/' . $uploaded_image->filename;
        
        $resized_file = $this->photos_path . '/' . $uploaded_image->url . '/' . $uploaded_image->resized_name;
 
        if (file_exists($file_path)) {
            unlink($file_path);
        }
 
        if (file_exists($resized_file)) {
            unlink($resized_file);
        }

        $imageGalleryTitle = $uploaded_image->ImageGallery->title;

        $imageTitle = $uploaded_image->original_name;

        if (!empty($uploaded_image)) {
            if ($uploaded_image->delete()) {
                $usersLogs->store($request, $imageGalleryTitle.' başlıklı resim galerisinden ' . $imageTitle . ' isimli resmi sildi.');
            }
            
        }
 
        return Response::json(['message' => 'File successfully deletedddd', 'Request' => $request->id], 200);
    }
}














