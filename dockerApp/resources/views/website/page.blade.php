@extends('website.app')

@section('title')

@endsection

@section('addcss')
<link rel="stylesheet" type="text/css" href="/css/richTextEditor/froala_style.css">  
@endsection

@section('content')



	<section class="jumbotron text-center">
		<div class="container">
			<h1 class="jumbotron-heading">{{$menu->title}}</h1>
			<p class="lead text-muted">{{$menu->subtitle}}</p>
		</div>
	</section>

	<div class="album py-5 bg-light">
		<div class="container">
			<div class="row">

				{!!$menu->Page->content!!}

			</div>
		</div>
	</div>

      <hr class="featurette-divider">
@endsection

@section('addjs')

@endsection


