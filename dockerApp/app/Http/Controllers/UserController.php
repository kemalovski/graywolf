<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\UsersLogsController;
use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view( 'dashboard.users.index', compact( 'users' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Auth $auth)
    {
        return view( 'dashboard.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UsersLogsController $usersLogs)
    {

        $this->validation($request);

        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password'])
        ]);

        $usersLogs->store($request, $request['name'].' isimli kullanıcıyı kayıt etti.');

        Session::flash('message', 'Kullanıcı kayıt oldu!');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view( 'dashboard.users.edit',compact('user') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, UsersLogsController $usersLogs)
    {

        // return $request->password;

        if ($request->password == '') {

            $request['password'] = $user->password;

        }else{
            $request['password'] = Hash::make($request['password']);
        }

        $this->validation($request, $user->id);

        $user->update([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => $request['password']
        ]);

        $usersLogs->store($request, $request['name'].' isimli kullanıcının bilgilerinde değişiklik yaptı.');

        return back()->with('message','Kullanıcı başarıyla değiştirildi');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, UsersLogsController $usersLogs, Request $request)
    {
        $name = $user->name;

        if ($user->delete()) {

            $usersLogs->store($request, $name.' isimli kullanıcı silindi.');

            return back()->with('message','Kullanıcı silindi!');
        }
    }

    /**
     * Validation requests.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function validation($request, $id = null)
    {

        return $this->validate( $request, 
            [
                'name' => 'required|max:191',
                'email' => 'required|max:191|unique:users,email,'.$id,
                'password' => 'required'
            ]
            ,[
                'name.required' => 'Adı soyadı boş bırakmayınız.',
                'name.max' => 'Ad soyad 191 karakterden fazla girmeyiniz.',
                'email.unique' => 'Bu e-postaya ait bir başka kullanıcı mevcut.',
                'email.required' => 'E-postayı boş bırakmayınız.',
                'email.max' => 'E-posta 191 karakterden fazla olamaz.',
                'password.required' => 'Şifreyi boş bırakmayınız.'
            ]
        );

    }
}
