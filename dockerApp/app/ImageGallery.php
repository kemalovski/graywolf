<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageGallery extends Model
{
    protected $fillable = [
        'title', 'url', 'description', 'images', 'tags','subtitle'
    ];

    public function images()
	{
		return $this->hasMany('App\Image');
	}

	public function blogs()
	{
		return $this->hasMany('App\Blog');
	}

	public function pages()
	{
		return $this->hasMany('App\Page');
	}

}
