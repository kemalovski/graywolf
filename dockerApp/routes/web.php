<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

// Route::get( '/{category}/{content}', function () {
//     return 'Hello World';
// });
*/

// ##################################### related to elasticsearch #####################################
Route::prefix('elasticsearch')->group(function(){
	Route::get('test', ['uses' => 'ClientController@elasticsearchTest']);
});


Route::get('/phpinfo', function() {
    exit(phpinfo());
});


// ##################################### Gray Wolf #####################################


// ##################################### Dashboard #####################################

Route::get('graywolf/dashboard', 'DashboardController@show')->name('dashboard.show')->middleware('auth');


// #####################################   Blogs   #####################################

Route::get('graywolf/blog/save', 'BlogController@showForm')->name('blog.showForm')->middleware('auth');

Route::post('graywolf/blog/save', 'BlogController@save')->middleware('auth');

Route::get('graywolf/blog/view', 'BlogController@view')->name('blog.view')->middleware('auth');

Route::post('graywolf/blog/remove', 'BlogController@remove')->middleware('auth');

Route::get('graywolf/blog/edit/{id}', 'BlogController@showEditForm')->name('blog.showEditForm')->middleware('auth');

Route::post('graywolf/blog/edit/{id}', 'BlogController@editBlog')->middleware('auth');

Route::get('graywolf/blog/kategoriler', 'BlogCategoryController@index')->name('blog.categoryIndex')->middleware('auth');

Route::post('graywolf/blog/kategoriler', 'BlogCategoryController@store')->middleware('auth');

Route::post('graywolf/blog/kategoriler/remove', 'BlogCategoryController@destroy')->middleware('auth');

Route::get('graywolf/blog/kategoriler/edit/{id}', 'BlogCategoryController@edit')->name('blog.editCategoryIndex')->middleware('auth');

Route::post('graywolf/blog/kategoriler/edit/{id}', 'BlogCategoryController@update')->middleware('auth');

// ##################################### Menus #####################################

Route::get('graywolf/menu/view', 'MenuController@index')->name('menu.index')->middleware('auth');

Route::post('graywolf/menu/view', 'MenuController@store')->middleware('auth');

Route::post('graywolf/menu/remove', 'MenuController@destroy')->middleware('auth');

Route::get('graywolf/menu/edit/{id}', 'MenuController@showEditForm')->name('menu.showEditForm')->middleware('auth');

Route::post('graywolf/menu/edit/{id}', 'MenuController@editMenu')->middleware('auth');

Route::post('graywolf/menu/sorting', 'MenuController@sorting')->middleware('auth');


// ##################################### Pages #####################################

Route::resource('graywolf/page','PageController')->except( ['store'] )->middleware('auth');

Route::post('graywolf/page/create', 'PageController@store')->middleware('auth');


// ##################################### Image Gallery #####################################


Route::resource('graywolf/imagegallery/images','ImageController')->except( ['destroy'] )->middleware('auth');

Route::post('graywolf/imagegallery/images/destroy', 'ImageController@destroy')->middleware('auth');

Route::resource('graywolf/imagegallery','ImageGalleryController')->middleware('auth');


// ##################################### User Logs #####################################

Route::resource('graywolf/userslogs','UsersLogsController')->middleware('auth');



// ##################################### Visitors Contact #####################################

Route::resource('contact','ContactController');


// ##################################### Products #####################################


Route::resource('graywolf/products','ProductController')->middleware('auth');







// ##################################### Authentication #####################################

// Authentication Routes...
Route::get('graywolf/login', 'Auth\LoginController@showLoginForm')->name('login');

Route::post('graywolf/login', 'Auth\LoginController@login');

Route::post('graywolf/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('graywolf/register', 'Auth\RegisterController@showRegistrationForm')->name('register');

Route::post('graywolf/register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('graywolf/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');

Route::post('graywolf/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

Route::get('graywolf/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

Route::post('graywolf/password/reset', 'Auth\ResetPasswordController@reset');

Route::resource('graywolf/users','UserController')->middleware('auth');






// ##################################### WebSite #####################################

//redis ile alakalı //
Route::get('/', 'WebsiteController@index' )->name('website.index');



Route::get('menu/{id}', 'MenuController@showArticle')->where('id', '[0-9]+');
Route::get('showblog', 'MenuController@showBlog');

// Route::get('/', function(){
// 	$redis = app()->make('redis');
// 	// $redis->set("key1","asfas");
// 	return $redis->get('key1');
// });

//redis ile alakalı //






Route::get('/sayfa/{page}', 'WebsiteController@page' )->name('website.page');

Route::get('/{category}', 'WebsiteController@category' )->name('website.category');

Route::get('/{menu}/{blog}', 'WebsiteController@content' )->name('website.content');
















// php artisan make:model usersLogs -a





