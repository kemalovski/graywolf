var total_photos_counter = 0;
Dropzone.options.myDropzone = {
    uploadMultiple: true,
    parallelUploads: 2,
    maxFilesize: 16,
    previewTemplate: document.querySelector('#preview').innerHTML,
    addRemoveLinks: true,
    dictRemoveFile: 'Remove file',
    dictFileTooBig: 'Image is larger than 16MB',
    timeout: 10000,
 
    init: function () {

        

        
        var mockFile = { name: '58967f3f1e5475585f807e1934fbbc18561fea31qQ.jpeg' }; // here we get the file name and size as response 

        this.options.addedfile.call(this, mockFile);

        this.options.thumbnail.call(this, mockFile, "/resim_galerileri/deneme/58967f3f1e5475585f807e1934fbbc18561fea31qQ.jpeg");

        this.on("removedfile", function (file) {
            $.post({
                url: '/graywolf/imagegallery/images/destroy',
                data: {id: file.name, _token: $('[name="_token"]').val() },
                dataType: 'json',
                success: function (data) {
                    total_photos_counter--;
                    $("#counter").text("# " + total_photos_counter);
                }
            });
        });
    },
    success: function (file, done) {
        total_photos_counter++;
        $("#counter").text("# " + total_photos_counter);
    }
};