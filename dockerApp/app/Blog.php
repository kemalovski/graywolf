<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'title', 'url', 'description', 'image', 'content', 'categories', 'tags', 'subtitle', 'image_gallery_id'
    ];

    public function  BlogCategory(){
    	return $this->belongsToMany('App\BlogCategory')->withTimestamps();
    }

    public function ImageGallery()
	{
		return $this->belongsTo('App\ImageGallery');
	}

}
