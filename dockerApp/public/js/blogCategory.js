// nested sortable js
$().ready(function(){
	var ns = $('ol.sortable').nestedSortable({
		forcePlaceholderSize: true,
		handle: 'div',
		helper:	'clone',
		items: 'li',
		opacity: .6,
		placeholder: 'placeholder',
		revert: 250,
		tabSize: 25,
		tolerance: 'pointer',
		toleranceElement: '> div',
		maxLevels: 1,
		isTree: true,
		expandOnHover: 700,
		startCollapsed: false,
		change: function(){
			// console.log('Relocated item');
		}
	});
	
	$('.expandEditor').attr('title','Click to show/hide item editor');
	$('.disclose').attr('title','Click to show/hide children');
	$('.deleteMenu').attr('title', 'Click to delete item.');

	$('.disclose').on('click', function() {
		$(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
		$(this).toggleClass('ui-icon-plusthick').toggleClass('ui-icon-minusthick');
	});
	
	$('.expandEditor, .itemTitle').click(function(){
		var id = $(this).attr('data-id');
		$('#menuEdit'+id).toggle();
		$(this).toggleClass('ui-icon-triangle-1-n').toggleClass('ui-icon-triangle-1-s');
	});
	
	$('.deleteMenu').click(function(){
		var id = $(this).attr('data-id');
		console.log(id);
		// $('#menuItem_'+id).remove();
	});
		
	$('#serialize').click(function(){
		serialized = $('ol.sortable').nestedSortable('serialize');
		$('#serializeOutput').text(serialized+'\n\n');
	});

});		

function removeButtonClicked(blogCategoryId){
	$('#blogCategoryId').val(blogCategoryId);
}
$(document).ready(function() {
	$('#removeMenu').click(function(){
		postAndRedirect( '/graywolf/blog/kategoriler/remove', $('#blogCategoryId').val() );
	});
});
// bootstrap için validation scripti
(function() {
	'use strict';
	window.addEventListener('load', function() {
	  var form = document.getElementById('needs-validation');
	  form.addEventListener('submit', function(event) {
	    if (form.checkValidity() === false) {
	      event.preventDefault();
	      event.stopPropagation();
	    }
	    form.classList.add('was-validated');
	  }, false);
	}, false);
})();