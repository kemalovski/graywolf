<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin Panel">
    <meta name="author" content="Kemal Karaduman">
    <link rel="icon" href="favicon.ico">

    <title>@yield('title')</title>

    {{-- css start --}}
    
    <link rel="stylesheet" href="/css/bootstrap.min.css" >
    <link href="/css/dashboard.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome-4.7.0/css/font-awesome.min.css">
    @yield('addcss')

    {{-- css end --}}
  </head>

  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">{{ Auth::user()->name }}</a>
      <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Çıkış</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link {{ (\Request::route()->getName() == 'dashboard.show') ? 'active' : '' }}" href="/graywolf/dashboard">
                  <span data-feather="home"></span>
                  Gösterge Paneli 
                </a>
              </li>
              
              <li class="nav-item">
                <a class="nav-link {{ (\Request::route()->getName() == 'menu.index' || \Request::route()->getName() == 'menu.showEditForm') ? 'active' : '' }}" href="/graywolf/menu/view">
                  <span data-feather="align-justify"></span>
                  Menüler
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link {{ (\Request::route()->getName() == 'blog.showForm' || \Request::route()->getName() == 'blog.view' || \Request::route()->getName() == 'blog.showEditForm' || \Request::route()->getName() == 'blog.categoryIndex' || \Request::route()->getName() == 'blog.editCategoryIndex' ) ? 'active' : '' }}" href="/graywolf/blog/view">
                  <span data-feather="file"></span>
                  Bloglar
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link {{ (\Request::route()->getName() == 'page.create' || \Request::route()->getName() == 'page.index' || \Request::route()->getName() == 'page.edit' ) ? 'active' : '' }}" href="{{ route('page.index') }}">
                  <span data-feather="file-text"></span>
                  Sayfalar
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link {{ ( \Request::route()->getName() == 'imagegallery.index' || \Request::route()->getName() == 'imagegallery.create' || \Request::route()->getName() == 'imagegallery.edit' || \Request::route()->getName() == 'images.edit') ? 'active' : '' }}" href="{{ route('imagegallery.index') }}">
                  <span data-feather="image"></span>
                  Resim Galerileri
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link {{ ( \Request::route()->getName() == 'products.index' || \Request::route()->getName() == 'products.create') || \Request::route()->getName() == 'products.show' || \Request::route()->getName() == 'products.edit' ? 'active' : '' }}" href="{{ route('products.index') }}">
                  <span data-feather="box"></span>
                  Ürünler
                </a>
              </li>
              {{-- <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="users"></span>
                  Customers
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="bar-chart-2"></span>
                  Reports
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="layers"></span>
                  Integrations
                </a>
              </li> --}}
            </ul>

            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mb-0 text-muted">
              <span>Graywolf</span>
              <a class="d-flex align-items-center text-muted" href="#">
                <img src="/images/graywolf.png" style="width: 45px">
              </a>
            </h6>
            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link  {{  \Request::route()->getName() == 'userslogs.index' ? 'active' : '' }}" href="{{ route('userslogs.index') }}">
                  <span data-feather="briefcase"></span>
                  Kullanıcı logları
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link {{ ( \Request::route()->getName() == 'users.index' || \Request::route()->getName() == 'users.create' || \Request::route()->getName() == 'users.edit' ) ? 'active' : '' }}" href="{{ route('users.index') }}">
                  <span data-feather="users"></span>
                  Kullanıcılar
                </a>
              </li>
              {{-- <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Current month
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Last quarter
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Social engagement
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Year-end sale
                </a>
              </li> --}}
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          @yield('content')
        </main>
      </div>
    </div>

    

    <!-- js start -->
    <script src="/js/jquery-3.2.1.slim.min.js" ></script>
    <script src="/js/popper.min.js" ></script>
    <script src="/js/bootstrap.min.js" ></script>
    <script src="/js/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
    @yield('addjs')
    {{-- js end --}}
  </body>
</html>