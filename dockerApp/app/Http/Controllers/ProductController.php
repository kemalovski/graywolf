<?php

namespace App\Http\Controllers;

use App\ImageGallery;
use App\Product;
use App\Http\Controllers\UsersLogsController;
use Session;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        return view( 'dashboard.product.index', compact( 'products' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $imageGalleries = ImageGallery::pluck('title', 'id');

        return view( 'dashboard.product.create', compact( 'imageGalleries' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UsersLogsController $usersLogs)
    {
        $this->validation($request);

        $imageName = $request['url'].'.'.request()->imageFile->getClientOriginalExtension();

        $request->merge(['image' => $imageName]);

        request()->imageFile->move(public_path('images/products'), $imageName);

        if ( $request->image_gallery_id == 0 ) {
        
            $request->request->remove('image_gallery_id');

        }

        Product::create($request->all());

        $usersLogs->store($request, $request['title'].' başlıklı ürün kayıt etti.');

        Session::flash('message', 'Ürün kayıt oldu!');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $imageGalleries = ImageGallery::pluck('title', 'id');

        return view( 'dashboard.product.create', compact( 'imageGalleries', 'product' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, UsersLogsController $usersLogs)
    {

        $this->validation($request, $product->id);

        
        if (request()->imageFile) {

            if( file_exists( public_path('images/products') . '/' . $product->image ) ){

                unlink( public_path('images/products') . '/' . $product->image );

            }

            $image = $request['url'].'.'.request()->imageFile->getClientOriginalExtension();

            $request->merge(['image' => $image]);

            request()->imageFile->move(public_path('images/products'), $image);

            $product->image = $request->get('image');

        }

        $product->update($request->all());

        $usersLogs->store($request, $product->id.' idli ürün ve '.$product->title.' başlıklı ürün değiştirildi.');

        Session::flash('message', 'Ürün değiştirildi!');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product, UsersLogsController $usersLogs)
    {

        if( file_exists( public_path('images/products') . '/' . $product->image ) ){

            unlink( public_path('images/products') . '/' . $product->image );

        }

        if ($product->delete()) {

            $usersLogs->store($request, $product->title.' başlıklı ürün silindi.');

            return back()->with('message','Ürün silindi!');
        }
    }

    public function validation($request, $id = null)
    {
        return $this->validate( $request, 
            [
                'title' => 'required|max:100|unique:products,title,'.$id,
                'url' => 'required|max:150|unique:products,url,'.$id,
                'subtitle' => 'required',
                'content' => 'required',
                'description' => 'required',
                'tags' => 'max:300'
            ]
            ,[
                'title.required' => 'Başlığı boş bırakmayınız.',
                'title.max' => 'Başlığı 100 karakterden fazla girmeyiniz.',
                'title.unique' => 'Bu başlığa ait bir başka ürün mevcut.',

                'url.required' => 'Uzantıyı boş bırakmayınız.',
                'url.unique' => 'Bu uzantıya ait bir başka ürün mevcut.',
                'url.max' => 'Uzantı 150 karakterden fazla olamaz.',

                'subtitle.required' => 'Alt başlığı boş bırakmayınız.',

                'content.required' => 'İçeriği boş bırakmayınız.',

                'description.required' => 'Google için olan açıklamayı boş bırakmayınız.',

                'tags.max' => 'Etiketler 300 karakterden fazla olamaz.'
            ]
        );
    }
}
