<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $fillable = [
        'title', 'subtitle', 'description', 'image', 'url'
    ];

    public function  Blog(){
    	return $this->belongsToMany('App\Blog')->orderBy('created_at', 'desc');
    }
}
