<?php

namespace App\Http\Controllers;

use App\usersLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class UsersLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usersLogs = DB::table('users_logs')
                    ->orderBy('id', 'desc')
                    ->get();

        return view('dashboard.userLogs.index', compact( 'usersLogs' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $description)
    {
        
        if (Auth::check()) {
            usersLogs::create([
                'name' => Auth::user()->name,
                'email' => Auth::user()->email,
                'ip' => \Request::ip(),
                'description' => $description,
            ]);
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\usersLogs  $usersLogs
     * @return \Illuminate\Http\Response
     */
    public function show(usersLogs $usersLogs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\usersLogs  $usersLogs
     * @return \Illuminate\Http\Response
     */
    public function edit(usersLogs $usersLogs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\usersLogs  $usersLogs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, usersLogs $usersLogs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\usersLogs  $usersLogs
     * @return \Illuminate\Http\Response
     */
    public function destroy(usersLogs $usersLogs)
    {
        //
    }
}
