@extends('dashboard.app')

@section('content')
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Kullanıcı Logları</h1>
		
	</div>
	@if(Session::has('message'))
		<div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    	<span aria-hidden="true" style="font-size:20px">×</span>
		  	</button>
		  	{{ Session::get('message') }}
		</div>
	@endif
	<table id="datatables" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Ad</th>
                <th>E-posta</th>
                <th>IP</th>
                <th>Açıklama</th>
                <th>Oluşturulma Tarihi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($usersLogs as $log)
                <tr>
                    <td>{{ $log->id }}</td>
                    <td>{{ $log->name }}</td>
                    <td>{{ $log->email }}</td>
                    <td>{{ $log->ip }}</td>
                    <td>{{ $log->description }}</td>
                    <td>{{ $log->created_at }}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Id</th>
                <th>Ad</th>
                <th>E-posta</th>
                <th>IP</th>
                <th>Açıklama</th>
                <th>Oluşturulma Tarihi</th>
            </tr>
        </tfoot>
    </table>
	

@endsection

@section('addcss')
	<link rel="stylesheet" href="/css/dataTables.bootstrap4.min.css">
@endsection

@section('addjs')
	<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#datatables').DataTable({
		    	"order": [[ 3, "desc" ]]
		    });
		});
	</script>
@endsection

