<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('blog_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('up_id')->default(0);
            $table->integer('order')->default(0);
            $table->string('title', 254);
            $table->string('url')->unique();
            $table->timestamps();
        });

        Schema::create('blog_blog_category', function (Blueprint $table) {
            $table->integer('blog_category_id')->unsigned()->index();
            $table->integer('blog_id')->unsigned()->index();
            $table->timestamps();
            $table->foreign('blog_id')->references('id')->on('blogs')->onDelete('cascade');
            $table->foreign('blog_category_id')->references('id')->on('blog_categories')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_categories');
        Schema::dropIfExists('blog_category_blog');
    }
}
