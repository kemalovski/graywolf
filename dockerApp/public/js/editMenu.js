$().ready(function(){
	var ns = $('ol.sortable').nestedSortable({
		forcePlaceholderSize: true,
		handle: 'div',
		helper:	'clone',
		items: 'li',
		opacity: .6,
		placeholder: 'placeholder',
		revert: 250,
		tabSize: 25,
		tolerance: 'pointer',
		toleranceElement: '> div',
		maxLevels: 10,
		isTree: true,
		expandOnHover: 700,
		startCollapsed: false,
		change: function(){
			// console.log('Relocated item');
		}
	});
	
	$('.expandEditor').attr('title','Click to show/hide item editor');
	$('.disclose').attr('title','Click to show/hide children');
	$('.deleteMenu').attr('title', 'Click to delete item.');

	$('.disclose').on('click', function() {
		$(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
		$(this).toggleClass('ui-icon-plusthick').toggleClass('ui-icon-minusthick');
	});
	
	$('.expandEditor, .itemTitle').click(function(){
		var id = $(this).attr('data-id');
		$('#menuEdit'+id).toggle();
		$(this).toggleClass('ui-icon-triangle-1-n').toggleClass('ui-icon-triangle-1-s');
	});
	
	$('.deleteMenu').click(function(){
		var id = $(this).attr('data-id');
		console.log(id);
		// $('#menuItem_'+id).remove();
	});
		
	$('#serialize').click(function(){
		serialized = $('ol.sortable').nestedSortable('serialize');
		$('#serializeOutput').text(serialized+'\n\n');
	});

});			



function dump(arr,level) {
	var dumped_text = "";
	if(!level) level = 0;

	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";

	if(typeof(arr) == 'object') { //Array/Hashes/Objects
		for(var item in arr) {
			var value = arr[item];

			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Strings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}
function removeButtonClicked(blogId){
	$('#menuId').val(blogId);
}

$(document).ready(function() {
	$('#removeMenu').click(function(){
		postAndRedirect( '/graywolf/menu/remove', $('#menuId').val() );
	});
});
function slug(str) {
  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();

  // remove accents, swap ñ for n, etc
  var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñçşIĞğı·/_,:;";
  var to   = "aaaaaeeeeeiiiiooooouuuuncsiggi------";
  for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-'); // collapse dashes

  return str;
};
$("#title").keyup(function(){
    $("#url").val( slug( $(this).val() ) );
});
(function() {
	'use strict';
	window.addEventListener('load', function() {
	  var form = document.getElementById('needs-validation');
	  form.addEventListener('submit', function(event) {
	    if (form.checkValidity() === false) {
	      event.preventDefault();
	      event.stopPropagation();
	    }
	    form.classList.add('was-validated');
	  }, false);
	}, false);
})();

