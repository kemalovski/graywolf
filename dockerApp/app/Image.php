<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function ImageGallery()
	{
		return $this->belongsTo('App\ImageGallery');
	}

}
