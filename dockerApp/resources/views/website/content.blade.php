@extends('website.app')

@section('title')

@endsection

@section('metaDescription')

@endsection

@section('content')
  <div class="jumbotron">
    <h1>{{$blog->title}}</h1>
    <div>
      <img src="/images/blogs/{{$blog->image}}" class="float-left img-thumbnail contentImage" >
      <div>
        <h5>{{$blog->subtitle}}</h5>
        {!!$blog->content!!}
        <small class="text-muted">{{ \Carbon\Carbon::parse( $blog->created_at )->diffForHumans( \Carbon\Carbon::now() ) }} oluşturuldu.</small>
      </div>
    </div>
    <hr class="featurette-divider">
    <h3>{{$menu->title}}' in son 3 haberi</h3>

    <div class="card-deck">
      
      @foreach($menu->BlogCategory->Blog as $key => $blog)
        <div class="card">
          <a href="/{{$menu->url}}/{{$blog->url}}">
            <img class="card-img-top" src="/images/blogs/{{$blog->image}}" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">{{$blog->title}}</h5>
              <p class="card-text">{{$blog->description}}</p>
              <p class="card-text"><small class="text-muted">{{ \Carbon\Carbon::parse( $blog->created_at )->diffForHumans( \Carbon\Carbon::now() ) }}</small></p>
            </div>
          </a>
        </div>
      @endforeach
    </div>
  </div>

@endsection

@section('addjs')

@endsection