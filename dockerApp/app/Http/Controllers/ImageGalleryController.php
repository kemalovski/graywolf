<?php

namespace App\Http\Controllers;

use App\ImageGallery;
use Illuminate\Http\Request;
use Session;
use App\Image;
use App\Http\Controllers\UsersLogsController;

class ImageGalleryController extends Controller
{
    private $photos_path;
 
    public function __construct()
    {
        $this->photos_path = public_path('/resim_galerileri');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imageGalleries = ImageGallery::all();

        return view('dashboard.imageGallery.index', compact( 'imageGalleries' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.imageGallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UsersLogsController $usersLogs)
    {
        
        $this->validation($request);

        $imageGallery = ImageGallery::create($request->all());

        $usersLogs->store($request, $request['title'].' başlıklı resim galerisi kayıt etti.');

        return redirect('graywolf/imagegallery/images/'.$imageGallery->id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ImageGallery  $imageGallery
     * @return \Illuminate\Http\Response
     */
    public function show(ImageGallery $imageGallery)
    {
        return '3123';
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ImageGallery  $imageGallery
     * @return \Illuminate\Http\Response
     */
    public function edit(ImageGallery $imageGallery)
    {
        return '31231341';
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ImageGallery  $imageGallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ImageGallery $imageGallery)
    {
        return '35345234';
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ImageGallery  $imageGallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ImageGallery $imagegallery, UsersLogsController $usersLogs)
    {

        $imagegalleryId = $imagegallery->id;

        $uploaded_images = Image::where('image_gallery_id', $imagegalleryId)->get();
 
        if (!empty($uploaded_images)) {

            $path = $this->photos_path . '/' . $imagegallery->url;

            if(is_dir($path)){
                
                array_map( 'unlink', glob($path."/*") );
                
                rmdir($path);

            }

        }

        Image::where('image_gallery_id', $imagegalleryId)->delete();

        $title = $imagegallery->title;

        if ($imagegallery->delete()) {
            $usersLogs->store($request, $title.' başlıklı resim galerisini sildi.');
        }

        return back()->with('message','Resim Galerisi silindi!');

    }

    public function validation($request)
    {
        return $this->validate( $request, 
            [
                'title' => 'required|max:200',
                'url' => 'required|unique:image_galleries|max:150',
                'tags' => 'max:200',
                'description' => 'max:1000',
                'subtitle' => 'max:1000'
            ]
            ,[
                'title.required' => 'Başlığı boş bırakmayınız.',
                'title.max' => 'Başlık 200 karakterden uzun yazmayınız.', 
                'url.unique' => 'Uzantı zaten kayıtlı.', 
                'url.max' => 'Uzantı 150 karakterden uzun yazmayınız.', 
                'url.required' => 'Uzantıyı boş bırakmayınız.',
                'tags.max' => 'Etiketler 200 karakterden fazla olamaz.',
                'description.max' => 'Açıklama 1000 karakterden fazla olamaz.',
                'subtitle.max' => 'Alt Başlık 1000 karakterden fazla olamaz.'
            ]
        );
    }
}
















