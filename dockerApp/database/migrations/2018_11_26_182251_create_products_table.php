<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->mediumText('subtitle');
            $table->string('url', 150)->unique();
            $table->mediumText('description');
            $table->string('image', 100);
            $table->longText('content');
            $table->string('categories', 50)->nullable();
            $table->string('tags', 100)->nullable();
            $table->timestamps();
            $table->unsignedInteger('image_gallery_id')->unsigned()->nullable();
            $table->foreign('image_gallery_id')->references('id')->on('image_galleries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
