@extends('dashboard.app')

@section('content')
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<h1 class="h2">Menüler</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="dashboard">Menüler</a></li>
			</ol>
		</nav>
	</div>
	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	@if(Session::has('message'))
		<div class="alert alert-success fade in alert-dismissible show" style="margin-top:18px;">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    	<span aria-hidden="true" style="font-size:20px">×</span>
		  	</button>
		  	{{ Session::get('message') }}
		</div>
	@endif

	<div class="row">
		<div class="col-lg-6">
			<form id="needs-validation" novalidate method="post">
				@csrf
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label for="title">Başlık</label>
							<input type="text" maxlength="100" value="{{ old('title') }}{{ $menu->title }}" class="form-control" id="title" name="title" placeholder="Başlık giriniz." required>
			                <div class="invalid-feedback">
			                	Başlığı boş bırakmayınız.
			                </div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label for="url">Uzantı</label>
							<input type="text" maxlength="150" value="{{ old('url') }}{{ $menu->url }}" class="form-control" name="url" id="url" placeholder="Uzantı kendiliğinden oluşacaktır." required>
							<div class="invalid-feedback">
			                	Uzantıyı boş bırakmayınız.
			                </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label for="title">Alt Başlık</label>
							<input type="text" value="{{ old('subtitle') }}{{ $menu->subtitle }}" class="form-control" id="subtitle" name="subtitle" placeholder="Alt başlık giriniz." required>
			                <div class="invalid-feedback">
			                	Alt Başlığı boş bırakmayınız.
			                </div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label for="up_id">Üst Kategori</label>
							<select class="custom-select" name="up_id" id="up_id">
								<option value="{{$menu->up_id}}" selected>{{$menu->up_id}}</option>
								@foreach ($menus as $val)
									<option value="{{$val->id}}">{{$val->title}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label for="blog_categories_id">Blog Kategorileri</label>
							<select class="custom-select" name="blog_categories_id" id="blog_categories_id" >
								@if ( empty($menu->BlogCategory->id) )
									<option value="" selected>Bir Blog Kategorisi Seçiniz.</option>
								@else
									<option value="{{$menu->BlogCategory->id}}" selected>{{$menu->BlogCategory->title}}</option>
								@endif

								@foreach ($blogCategories as $blogCategory)
									<option value="{{$blogCategory->id}}">{{$blogCategory->title}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label for="pages_id">Sayfalar</label>
							<select class="custom-select" name="pages_id" id="pages_id" >
								@if ( empty($menu->Page->id) )
									<option value="" selected>Sayfa Seçiniz.</option>
								@else
									<option value="{{$menu->Page->id}}" selected>{{$menu->Page->title}}</option>
								@endif
								@foreach ($pages as $title => $id)
									<option value="{{$id}}">{{$title}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="description">Google için Açıklama</label>
							<textarea class="form-control" id="description" name="description" placeholder="Açıklama giriniz." rows="3" required>{{ old('description') }}{{ $menu->description }}</textarea>
							
							<div class="invalid-feedback">
			                	Açıklamayı boş bırakmayınız.
			                </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<button type="submit" class="btn btn-info col-lg-12" id="buttonSaveForm">Değiştir</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-6">
			{!! getMenuJson() !!}
			<button id="toArray" type="submit" class="btn btn-info col-lg-12" id="buttonSaveForm">Menüleri Sırala</button>
		</div>
	</div>
	
	<div class="modal fade" id="removeMenuModal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLongTitle">Menüyü Silme İşlemi</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        Evete tıklarsanız, Menüyü silmiş olacaksınız. Emin misiniz?
	        <input type="hidden" id="menuId" >
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="hayir">Hayır</button>
	        <button type="button" class="btn btn-primary" id="removeMenu" >Evet</button>
	      </div>
	    </div>
	  </div>
	</div>

	<button type="button" class="btn btn-primary d-none" id="menusOrderingModalButton" data-toggle="modal" data-target="#menusOrderingModal">
	  Open modal
	</button>

	<div class="modal fade" id="menusOrderingModal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      
	      <img src="/images/tenor.gif">
	      
	    </div>
	  </div>
	</div>
@endsection

@section('addcss')
	<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/css/nestedSortable.css">
@endsection

@section('addjs')
	<script src="/js/jquery.min.js"></script>
	<script src="/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/jquery.mjs.nestedSortable.js"></script>
	<script type="text/javascript" src="/js/editMenu.js"></script>
	<script type="text/javascript">
		$('#toArray').click(function(e){
			arraied = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});
			
			setNestedMenuOrder(arraied);
		});
		$('#toHierarchy').click(function(e){
			hiered = $('ol.sortable').nestedSortable('toHierarchy', {startDepthCount: 0});
			console.log(hiered);
			hiered = dump(hiered);
			(typeof($('#toHierarchyOutput')[0].textContent) != 'undefined') ?
			$('#toHierarchyOutput')[0].textContent = hiered : $('#toHierarchyOutput')[0].innerText = hiered;
		});
		function setNestedMenuOrder(arraied){
			$.ajax({
		        type: 'POST',
		        url: '/graywolf/menu/sorting',
		        ascyn: false,
		        cache: false,
		        data: { "_token": "{{ csrf_token() }}", sorting : JSON.stringify(arraied)},
				        

		         
		        beforeSend: function() {
		        	$('#menusOrderingModalButton').click();
			    },
		        success: function (jsonResult) {
		            console.log(jsonResult);
		            $('#menusOrderingModalButton').click();
		        },
		        error: function (request, status, error) {
			        console.log(request.responseText);
			    }
		    });
		}
		function postAndRedirect(url, postData)
		{
		    var postFormStr = "<form method='POST' action='" + url + "'>\n";
		    for (var key in postData)
		    {
		        if (postData.hasOwnProperty(key))
		        {
		            postFormStr += '@csrf'+"<input type='hidden' name='id' value='" + postData + "'></input>";
		        }
		    }
		    postFormStr += "</form>";

		    var formElement = $(postFormStr);

		    $('body').append(formElement);
		    $(formElement).submit();
		}
	</script>
@endsection