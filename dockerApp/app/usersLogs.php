<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usersLogs extends Model
{
	protected $fillable = [
        'name', 'email', 'url', 'ip', 'description'
    ];
}
